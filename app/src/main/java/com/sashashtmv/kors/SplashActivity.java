package com.sashashtmv.kors;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.model.Product;
import com.sashashtmv.kors.model.impl.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class SplashActivity extends AppCompatActivity {
    private ImageView mImageView;
    private Animation mAnimation;
    private ConstraintLayout mConstraintLayout;
   static List<Category> listCategories;
   static List<Product> listProducts = new ArrayList<>();
    private APIInterface apiInterface;

    //констатнта для длительности отображения экрана приветствия
    private static final int SPLASH_DURATION = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        mConstraintLayout = findViewById(R.id.splash_layout);
        mImageView = findViewById(R.id.iv_splash_icon);
        mAnimation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate);
        getCategoriesRetrofit2Api();
    }

    //создаем отдельный поток и в нем отображаем прогрессбар
    //применяем анимацию к изображению и создаем слушателя к изображению
    private void initFunctionality(){
        mConstraintLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mImageView.startAnimation(mAnimation);
                mAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        // по окончании анимации будем запускать главный экран приложения
                        Intent intent = new Intent(SplashActivity.this,  MainActivity.class);
                        SplashActivity.this.startActivity(intent);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }, SPLASH_DURATION);
    }

    private void getCategoriesRetrofit2Api() {
        try {
            Call<Object> call1 = apiInterface.getCategories();
            call1.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            listCategories = new ArrayList<>();
                            JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("category");

                            for (int i = 0; i < array.length(); i++) {
                                String temp = array.getString(i).toString();
                                object = new JSONObject(temp);
                                boolean isPublished = object.getBoolean("is_published");
                                String categoryName = object.getString("name");
                                int categoryId = object.getInt("id");
                                Category category = new Category(categoryId, categoryName, isPublished);
                                if (isPublished) {
                                    listCategories.add(category);
                                }
                            }
                            if (!listCategories.isEmpty()) {
                                for (Category c : listCategories) {
                                    getProductsRetrofit2Api(c.getId());
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Введенные данные не верны", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + " - faile");
                }
            });
        } catch (Exception e) {

        }
    }

    private synchronized void getProductsRetrofit2Api(int categoryId) {
        try {
            Call<Object> call = apiInterface.getProducts(String.valueOf(categoryId));
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("product");

                            for (int i = 0; i < array.length(); i++) {
                                String temp = array.getString(i).toString();
                                object = new JSONObject(temp);
                                int productId = object.getInt("id");
                                String productName = object.getString("name");
                                String productPrice = String.valueOf(object.getDouble("price"));
                                String productOldPrice = String.valueOf(object.toString().contains("old_price") ? object.getDouble("old_price") : 0);
                                boolean isNew = object.getBoolean("is_new");
                                boolean isStock = object.getBoolean("in_stock");
                                int sku = object.getInt("sku");
                                double categoryId = object.getInt("category_id");
                                String categoryName = object.getString("category_name");
                                String description = object.getString("description");
                                boolean isPublished = object.getBoolean("is_published");
                                JSONArray images = object.getJSONArray("images");
                                String url = "";
                                List<String> urls = new ArrayList<>();
                                for (int j = 0; j < images.length(); j++) {
                                    JSONObject imageUrl = images.getJSONObject(j);
                                    if (imageUrl.getBoolean("is_main")) {
                                        url = "https://kors.fun".concat(imageUrl.getString("url"));
                                        urls.add(0, url);
                                    }else {
                                        url = "https://kors.fun".concat(imageUrl.getString("url"));
                                        urls.add(url);
                                    }
                                }
                                Product product = new Product(productId, productName, productPrice, productOldPrice, isNew, isStock, sku, categoryId, categoryName, description, isPublished, urls);
                                boolean match = false;
                                for (Product item : listProducts) {
                                    if (item.getProductId() == product.getProductId()) {
                                        match = true;
                                        break;
                                    }
                                }
                                if (!match)
                                    listProducts.add(product);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Введенные данные не верны", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initFunctionality();
    }
}