package com.sashashtmv.kors;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.google.gson.Gson;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.fragments.AboutUsFragment;
import com.sashashtmv.kors.fragments.CartProductFragment;
import com.sashashtmv.kors.fragments.ChangePasswordFragment;
import com.sashashtmv.kors.fragments.CompleteOrderFragment;
import com.sashashtmv.kors.fragments.ContactsFragment;
import com.sashashtmv.kors.fragments.PrivatePoliticFragment;
import com.sashashtmv.kors.fragments.EditDataFragment;
import com.sashashtmv.kors.fragments.EnterFragment;
import com.sashashtmv.kors.fragments.ListProductsFragment;
import com.sashashtmv.kors.fragments.MyOrdersFragment;
import com.sashashtmv.kors.fragments.PasswordRecoveryFragment;
import com.sashashtmv.kors.fragments.ProfileFragment;
import com.sashashtmv.kors.fragments.DeliveryFragment;
import com.sashashtmv.kors.fragments.RegistrationFragment;
import com.sashashtmv.kors.fragments.ReviewsFragment;
import com.sashashtmv.kors.fragments.PublicOfertFragment;
import com.sashashtmv.kors.fragments.StartFragment;
import com.sashashtmv.kors.fragments.SupportFragment;
import com.sashashtmv.kors.fragments.ValidationFragment;
import com.sashashtmv.kors.fragments.WithdrawalOfFundsFragment;
import com.sashashtmv.kors.model.Article;
import com.sashashtmv.kors.model.Order;
import com.sashashtmv.kors.model.PreferenceHelper;
import com.sashashtmv.kors.model.Product;
import com.sashashtmv.kors.model.User;
import com.sashashtmv.kors.model.impl.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import retrofit2.Call;

import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements StartFragment.Callbacks, EnterFragment.OnFragmentEnterListener,
        RegistrationFragment.OnFragmentRegistrationListener, ListProductsFragment.OnCartProductFragmentListener, MyOrdersFragment.OnCompleteOrderFragmentListener,
        CompleteOrderFragment.OnFragmentCompleteOrderListener, ValidationFragment.OnFragmentValidationListener, EditDataFragment.OnFragmentEditDataListener {

    public static final int ACTIVITY_RESULT_AUTH = 5;
    FragmentManager mFragmentManager;
    StartFragment startFragment;
    EnterFragment enterFragment;
    RegistrationFragment registrationFragment;
    PrivatePoliticFragment privatePoliticFragment;
    DeliveryFragment deliveryFragment;
    AboutUsFragment aboutUsFragment;
    SupportFragment supportFragment;
    PasswordRecoveryFragment passwordRecoveryFragment;
    ProfileFragment profileFragment;
    MyOrdersFragment myOrdersFragment;
    ChangePasswordFragment changePasswordFragment;
    EditDataFragment editDataFragment;
    WithdrawalOfFundsFragment withdrawalOfFundsFragment;
    ReviewsFragment reviewsFragment;
    ListProductsFragment listProductFragment;
    private CallbackManager callbackManager;
    private PublicOfertFragment publicOfertFragment;

    private List<Article> listArticle;
    public List<Product> listOrder;
    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;
    private ValidationFragment validationFragment;
    List<Category> listCategories;
    List<Product> listProducts = new ArrayList<>();
    List<Order> listOrders = new ArrayList<>();
    private CartProductFragment cartProductFragment;
    private CompleteOrderFragment completeOrderFragment;
    public static boolean access = false;
    private ContactsFragment contactsFragment;
    private SharedPreferences sharedPref;
    private User user;
    private String mParole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        callbackManager = CallbackManager.Factory.create();
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        listArticle = new ArrayList<>();
        listOrder = new ArrayList<>();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        PreferenceHelper.getInstance().init(this);
        mPreferenceHelper = PreferenceHelper.getInstance();
        listCategories = SplashActivity.listCategories;
        listProducts = SplashActivity.listProducts;
        mFragmentManager = getFragmentManager();
        onFragmentStart();
        getArticles(mPreferenceHelper.getString("access_token"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        Fragment f = mFragmentManager.findFragmentById(R.id.container);
        if (f instanceof ProfileFragment) {
            onFragmentStart();
        } else if (f instanceof StartFragment) {
            mFragmentManager.popBackStack("StartFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            int count = getFragmentManager().getBackStackEntryCount();

            if (count == 0) {
                super.onBackPressed();
            } else {
                getFragmentManager().popBackStack();
            }
        }
    }

    @Override
    public void onCreateAllShopsFragment(String title) {
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < listProducts.size(); i++) {
            if (title.toLowerCase().equals(listProducts.get(i).getCategoryName().toLowerCase())) {
                products.add(listProducts.get(i));
            }
            if (title.equals("Мои магазины") && listProducts.get(i).isPublished()) {
                products.add(listProducts.get(i));
            }
        }
        if (title.equals("Все магазины")) {
            products = listProducts;
        }
        listProductFragment = ListProductsFragment.newInstance(title, products);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, listProductFragment, "products")
                .addToBackStack(null)
                .commit();
    }

        @Override
        public void onCreateReviewsFragment () {
            reviewsFragment = ReviewsFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, reviewsFragment, "reviews")
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateEnterFragment () {
            enterFragment = EnterFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, enterFragment, "enterfragment")
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreatePrivatePoliticFragment () {
            privatePoliticFragment = PrivatePoliticFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, privatePoliticFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateDeliveryFragment () {
            deliveryFragment = DeliveryFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, deliveryFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreatePublicOfertFragment () {
            publicOfertFragment = PublicOfertFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, publicOfertFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateAboutUsFragment () {
            aboutUsFragment = AboutUsFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, aboutUsFragment)
                    .addToBackStack(null)
                    .commit();
        }


        @Override
        public void onSupportFragment () {
            supportFragment = SupportFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, supportFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onFragmentStart () {
            startFragment = StartFragment.newInstance(listCategories);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, startFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onFragmentRegistration () {
            registrationFragment = RegistrationFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, registrationFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onRecoveryPasswordFragment () {
            passwordRecoveryFragment = PasswordRecoveryFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, passwordRecoveryFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateProfileFragment (String parole){
            if (parole != null) mParole = parole;
            profileFragment = ProfileFragment.newInstance(listOrders, this, user, mParole);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, profileFragment, "profile")
                    .commit();
        }

        @Override
        public void onCreateMyOrdersFragment () {
            getOrders();
            myOrdersFragment = MyOrdersFragment.newInstance(listOrder, this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, myOrdersFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateChangePasswordFragment () {
            changePasswordFragment = ChangePasswordFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, changePasswordFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateEditDataFragment () {
            mFragmentManager.popBackStackImmediate();
            editDataFragment = EditDataFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, editDataFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateWithdrawalOfFundsFragment () {
            withdrawalOfFundsFragment = WithdrawalOfFundsFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, withdrawalOfFundsFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onFragmentValidation () {
            validationFragment = ValidationFragment.newInstance(this);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, validationFragment, "validation")
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateContactsFragment () {
            contactsFragment = ContactsFragment.newInstance();
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, contactsFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateCartProductFragment (Product product){
            cartProductFragment = CartProductFragment.newInstance(product);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, cartProductFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onCreateCompleteOrderFragment (List < Product > list) {
            listOrder = list;
            completeOrderFragment = CompleteOrderFragment.newInstance(list, user);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, completeOrderFragment)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onClearOrder () {
            for (Product product : listOrder) {
                sharedPref.edit().putInt(String.valueOf(product.getProductId()), 0).apply();
            }
            listOrder.clear();
        }

        @Override
        public void getOrdersRetrofit2Api () {
            String token = mPreferenceHelper.getString("access_token");
            try {
                Call<Object> call = apiInterface.getUserOrders("Bearer " + token);
                call.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        JSONObject object = null;
                        if (response.isSuccessful() && response.body() != null) {
                            try {
                                JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("sale");

                                for (int i = 0; i < array.length(); i++) {
                                    String temp = array.getString(i).toString();
                                    object = new JSONObject(temp);
                                    String productId = object.getString("id");
                                    String date = object.getString("datetime");
                                    String status = new JSONObject(object.getString("status")).getString("name");
                                    JSONArray arrayProducts = object.getJSONArray("products");
                                    for (int j = 0; j < arrayProducts.length(); j++) {
                                        String productName = new JSONObject(arrayProducts.getString(j)).getString("name");
                                        String productPrice = new JSONObject(arrayProducts.getString(j)).getString("price");
                                        String productCount = new JSONObject(arrayProducts.getString(j)).getString("amount");
                                        Order order = new Order(date, productId, productName, productPrice, status, productCount);
                                        if (!listOrders.contains(order))
                                            listOrders.add(0, order);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Введенные данные не верны", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                    }
                });
            } catch (Exception e) {
            }
        }

        @Override
        public void getUserInfo (String parole){
            mParole = parole;
            String token = mPreferenceHelper.getString("access_token");
            try {
                Call<Object> call = apiInterface.getUserInfo("Bearer " + token);
                call.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        JSONObject object = null;
                        if (response.isSuccessful() && response.body() != null) {
                            try {
                                object = new JSONObject(response.body().toString()).getJSONObject("user");
                                String name = StringUtils.capitalize(object.getString("name"));
                                String surname = StringUtils.capitalize(object.getString("surname"));
                                String patronymic = StringUtils.capitalize(object.getString("patronymic"));
                                String phone = object.getString("phone");
                                String email = object.getString("email");

                                user = new User(email, phone, name, surname, patronymic);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Введенные данные не верны", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                    }
                });
            } catch (Exception e) {
            }
        }

        private void getArticles (String token){
            try {
                Call<Object> call1 = apiInterface.getArticles("Bearer " + token, "blog");
                call1.enqueue(new Callback<Object>() {

                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        JSONObject object = null;

                        try {
                            if (response.isSuccessful() && response.body() != null) {
                                JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("articles");
                                for (int i = 0; i < array.length(); i++) {
                                    String temp = array.getString(i).toString();
                                    object = new JSONObject(temp);
                                    String title = object.getString("title");
                                    String text = object.getString("text");
                                    String imageUrl = object.getString("image");

                                    text = text.replace("><", "");
                                    String[] mas = text.split("\n");
                                    text = "";
                                    for (int j = 0; j < mas.length; j++) {
                                        String str = mas[j].replace("><", "");
                                        if (!str.contains("<div") & str.contains("<") & str.contains(">") & str.indexOf(">") < str.lastIndexOf("<")) {
                                            str = str.substring(str.indexOf(">") + 1, (str.lastIndexOf("<")));
                                            str = str.replace("</strong>", "");
                                            text = text + str + "\n";
                                        }

                                    }
                                    URL url;
                                    if (imageUrl.startsWith("/")) {
                                        url = new URL("https://tc.orido.org" + imageUrl);

                                    } else url = new URL(imageUrl);

                                    listArticle.add(new Article(title, text, url));

                                }
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getBasketSize () {
            getOrders();
            return listOrder.size();
        }

        private void getOrders () {
            listOrder.clear();
            for (Product product : listProducts) {
                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                int count = sharedPref.getInt(String.valueOf(product.getProductId()), 0);
                if (count > 0) {
                    listOrder.add(product);
                }
            }
        }

        @Override
        public void getBalance () {
            String token = mPreferenceHelper.getString("access_token");
            try {
                Call<Object> call1 = apiInterface.getBalance("Bearer " + token);
                call1.enqueue(new Callback<Object>() {

                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        JSONObject object = null;

                        try {
                            if (response.isSuccessful() && response.body() != null) {
                                object = new JSONObject(response.body().toString());
                                String status = object.getString("status");
                                String ready_to_pay = object.getString("ready_to_pay");
                                String pending_to_pay = object.getString("pending_to_pay");
                                if (status.equals("true")) {
                                    mPreferenceHelper.putString("ready_to_pay", ready_to_pay);
                                    mPreferenceHelper.putString("pending_to_pay", pending_to_pay);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        public void withdrawals (String sum, String type, String number){
            String token = mPreferenceHelper.getString("access_token");
            try {
                Call<Object> call1 = apiInterface.withdrawals("Bearer " + token, sum, type, number);
                call1.enqueue(new Callback<Object>() {

                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        JSONObject object = null;

                        try {
                            if (response.isSuccessful() && response.body() != null) {

                                object = new JSONObject(response.body().toString());
                                String status = object.getString("status");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
