package com.sashashtmv.kors.converter;

import com.sashashtmv.kors.model.AreaModel;
import com.sashashtmv.kors.model.CityModel;
import com.sashashtmv.kors.model.PunktModel;
import com.sashashtmv.kors.response.AreaResponse;
import com.sashashtmv.kors.response.CityResponse;
import com.sashashtmv.kors.response.PunktResponse;

import java.util.ArrayList;
import java.util.List;

public class ModelConverter {

    public static AreaModel convertToAreaModel(AreaResponse response) {
        AreaModel areaModel = new AreaModel();

        areaModel.setAreasCenter(response.getAreasCenter());
        areaModel.setDescription(response.getDescription());
        areaModel.setRef(response.getRef());

        return areaModel;
    }

    public static List<AreaModel> convertToListAreaModel(List<AreaResponse> responseList) {
        List<AreaModel> list = new ArrayList<>();

        for (AreaResponse response : responseList) {
            list.add(convertToAreaModel(response));
        }
        return list;
    }

    public static CityModel convertToCityModel(CityResponse response) {
        CityModel cityModel = new CityModel();

        cityModel.setArea(response.getArea());
        cityModel.setCityID(response.getCityID());
        cityModel.setDescription(response.getDescription());
        cityModel.setSettlementType(response.getSettlementType());
        cityModel.setSettlementTypeDescription(response.getSettlementTypeDescription());
        cityModel.setRef(response.getRef());

        return cityModel;
    }

    public static List<CityModel> convertToListCityModel(List<CityResponse> responseList) {
        List<CityModel> list = new ArrayList<>();

        for (CityResponse response : responseList) {
            list.add(convertToCityModel(response));
        }
        return list;
    }

    public static PunktModel convertToPunktModel(PunktResponse response) {
        PunktModel punktModel = new PunktModel();
        punktModel.setCityDescription(response.getCityDescription());
        punktModel.setNumber(response.getNumber());
        punktModel.setTypeOfWarehouse(response.getTypeOfWarehouse());
        punktModel.setDescription(response.getDescription());
        punktModel.setRef(response.getRef());
        punktModel.setPhone(response.getPhone());

        return punktModel;
    }

    public static List<PunktModel> convertToListPunktModel(List<PunktResponse> responseList) {
        List<PunktModel> list = new ArrayList<>();

        for (PunktResponse response : responseList) {
            list.add(convertToPunktModel(response));
        }
        return list;
    }
}
