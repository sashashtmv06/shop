package com.sashashtmv.kors.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.model.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import android.app.Fragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationFragment extends Fragment {

    private EditText mName;
    private EditText mLastName;
    private EditText mTelephone;
    private EditText mPassword;

    private EditText mPatronymic;
    private Button mNext;
    public String fcm_token;

    static Context mContext;

    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;


    private OnFragmentRegistrationListener mListener;

    public RegistrationFragment() {
        // Required empty public constructor
    }

    public static RegistrationFragment newInstance(Context context) {
        RegistrationFragment fragment = new RegistrationFragment();
        mContext = context;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);

        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();

        mTelephone = view.findViewById(R.id.et_telephone);
        mPassword = view.findViewById(R.id.et_parole);
        mName = view.findViewById(R.id.et_name);
        mLastName = view.findViewById(R.id.et_last_name);
        mPatronymic = view.findViewById(R.id.et_patronymic);
        mNext = view.findViewById(R.id.bt_next);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        mNext.setOnClickListener(mOnEnterClickListener);
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String token = task.getResult();
                        fcm_token = token;
                    }
                });

        mTelephone.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            private boolean backspacingFlag = false;
            private boolean editedFlag = false;
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursorComplement = s.length() - mTelephone.getSelectionStart();
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");
                if (!editedFlag) {
                    if (phone.length() >= 5 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5);
                        if (phone.length() >= 10)
                            ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5, 8) + "-" + phone.substring(8, 10) + "-" + phone.substring(10);
                        else if (phone.length() >= 8)
                            ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5, 8) + "-" + phone.substring(8);
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    } else if (phone.length() > 2 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2);
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    } else if (phone.length() <= 2) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(";
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    }
                } else {
                    editedFlag = false;
                }
            }
        });
        return view;
    }

    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (checkValidation()) {
                if (CommonMethod.isNetworkAvailable(mContext)) {
                    sendVerifyCode("+".concat(mTelephone.getText().toString().replaceAll("[^\\d]", "")));
                    validationPhone("Мы отправили пароль на указанный Вами телефон. Введите его в поле ниже", getActivity());
                } else
                    CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
            }
        }
    };

    private void loginRetrofit2Api(String phone, String password, String name, String lastName, String patronymic) {
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("phone", phone);
            paramObject.put("password", password);
            paramObject.put("name", name);
            paramObject.put("surname", lastName);
            paramObject.put("patronymic", patronymic);
            paramObject.put("device_token", Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID));
            paramObject.put("fcm_token", fcm_token);
            Call<Object> call1 = apiInterface.createUser(phone, password, name, lastName, patronymic, Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID), fcm_token);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            boolean access = object.getBoolean("status");
                            String token = object.getString("token");
                            String message = object.getString("message");
                            if (access && token != null) {
                                mPreferenceHelper.putString("access_token", token);
                                mListener.getOrdersRetrofit2Api();
                                mListener.getUserInfo(password);
                                MainActivity.access = true;
                                onButtonPressed();
                                Toast.makeText(getActivity(), "Вы успешно зарегистрированы", Toast.LENGTH_LONG).show();
                                //startActivity(new Intent(AuthActivity.this, MainActivity.class));
                            } else if (!access) {
                                ((MainActivity) getActivity()).onFragmentValidation();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Возможно Ваш номер уже есть в базе, попробуйте восстановить пароль", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(mContext, "Ваш номер телефона или почта уже используются, введите другие данные", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {

        }
    }

    public void validationPhone(String message, Activity context) {
        final String phone = "+".concat(mTelephone.getText().toString().replaceAll("[^\\d]", ""));
        final EditText input = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        checkCode(phone, input.getText().toString());
                    }
                });
        try {
            AlertDialog alert = builder.create();
            alert.setView(input);
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendVerifyCode(String phone) {
        try {
            Call<Object> call1 = apiInterface.sendVerifyCode(phone);
            call1.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null && response.code() == 0) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            boolean status = object.getBoolean("status");
                            if (status) {
                                Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Номер телефона не верен", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Номер телефона не верен", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkCode(String phone, String code) {
        try {
            Call<Object> call1 = apiInterface.createValidation(phone, code);
            call1.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            boolean status = object.getBoolean("status");
                            if (status) {
                                if (CommonMethod.isNetworkAvailable(mContext))
                                    loginRetrofit2Api("+".concat(mTelephone.getText().toString().replaceAll("[^\\d]", "")), mPassword.getText().toString(),
                                            mName.getText().toString(), mLastName.getText().toString(), mPatronymic.getText().toString());
                                else
                                    CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
                                Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Код не верен", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Код не верен", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean checkValidation() {

        if (mName.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Имя нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mPassword.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Пароль нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mPassword.getText().toString().trim().length() < 7) {
            CommonMethod.showAlert("Пароль не может быть меньше 7 символов", (MainActivity) mContext);
            return false;
        } else if (mTelephone.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Телефон нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mLastName.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Фамилию нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        }
        return true;
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            try {
                builder.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentStart();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentRegistrationListener) {
            mListener = (OnFragmentRegistrationListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentRegistrationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentRegistrationListener {
        void getOrdersRetrofit2Api();

        void getUserInfo(String parole);

        void onFragmentStart();
    }
}
