package com.sashashtmv.kors.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;

import android.app.Fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.adapters.ReviewListAdapter;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.model.PreferenceHelper;
import com.sashashtmv.kors.model.Review;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class ReviewsFragment extends Fragment {
    private static Context mContext;
    private EditText review;
    private Button sendReview;
    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ReviewListAdapter adapter;


    public ReviewsFragment() {
        // Required empty public constructor
    }


    public static ReviewsFragment newInstance(Context context) {
        ReviewsFragment fragment = new ReviewsFragment();
        mContext = context;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();
        review = view.findViewById(R.id.et_review);
        sendReview = view.findViewById(R.id.bt_send);

        recyclerView = view.findViewById(R.id.rv_my_reviews);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);


        getReviews(mPreferenceHelper.getString("access_token"));

        sendReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed();
                review.setText("");
                ((MainActivity) mContext).onBackPressed();
            }
        });

        return view;
    }

    private void sendReview(String text) {
        try {
            String token = mPreferenceHelper.getString("access_token");
            Call<Object> call1 = apiInterface.sendReview("Bearer " + token, text);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: review sent" + response.code() + response.body());
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null && response.code() == 0) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getReviews(String token) {
        try {
            Call<Object> call1 = apiInterface.getReviews("Bearer " + token);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: review get" + response.code() + response.body());
                    JSONObject object = null;
                    List<Review> list = new ArrayList<>();
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            JSONArray array = new JSONObject(new Gson().toJson(response.body())).getJSONArray("reviews");

                            for (int i = 0; i < array.length(); i++) {
                                String temp = array.getString(i).toString();
                                object = new JSONObject(temp);
                                String name = object.getString("user");
                                String text = object.getString("text");
                                String date = object.getString("date");

                                list.add(new Review(name, text, date));
                                Log.i(TAG, "onResponse: review - " + name + "; " + text + "; " + date);

                            }
                            adapter = new ReviewListAdapter(mContext, list);
                            recyclerView.setAdapter(adapter);
                            recyclerView.setLayoutManager(layoutManager);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean checkValidation() {

        if (review.getText().toString().trim().equals("")) {
            ReviewsFragment.CommonMethod.showAlert("Напишите ваш отзыв", (MainActivity) mContext);
            return false;
        }
        return true;
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            try {
                AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (!ReviewsFragment.CommonMethod.isNetworkAvailable(mContext)) {
            ReviewsFragment.CommonMethod.showAlert("Подключите интернет", (MainActivity) mContext);
        } else if (checkValidation()) {
            sendReview(review.getText().toString());
            ReviewsFragment.CommonMethod.showAlert("Ваш отзыв направлен на модерацию. Спасибо за Вашу оценку нашей работы.", (MainActivity) mContext);
        }
    }

}
