package com.sashashtmv.kors.fragments;

import com.sashashtmv.kors.model.AreaModel;
import com.sashashtmv.kors.model.CityModel;
import com.sashashtmv.kors.model.PunktModel;

import java.util.List;





public interface ContractMain {
    interface IViewMain {

        void setAreas(List<AreaModel> list);

        void setCities(List<CityModel> list);
        void setPunkts(List<PunktModel> list);

        void showError();
    }

    interface IPresenerMain {

        void getAreas();

        void getCities(String regionRef);

        void  getPunkt(String nameCity);
    }
}
