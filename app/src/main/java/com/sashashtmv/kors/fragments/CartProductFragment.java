package com.sashashtmv.kors.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.app.Fragment;

import android.os.Parcelable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.model.Product;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicBoolean;


public class CartProductFragment extends Fragment {
    private static final String PRODUCT_KEY = "product";
    ImageButton back;
    Button addBasket;
    Product mProduct;
    EditText mCount;
    TextView mOldPrice;
    TextView mPrice;
    TextView mDescribe;
    TextView mName_Product;
    TextView mInStock;
    ImageView mImageProduct;
    ImageButton mContinue;
    ImageButton mBack;
    int position = 0;
    private int basketSize = 0;
    private Button basket;
    private Button circle;
//    Context mContext;


    public CartProductFragment() {
        // Required empty public constructor
    }


    public static CartProductFragment newInstance(Product product) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(PRODUCT_KEY, (Serializable) product);
        CartProductFragment fragment = new CartProductFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart_product, container, false);
        back = view.findViewById(R.id.ib_back);
        addBasket = view.findViewById(R.id.bt_add_basket);
        mCount = view.findViewById(R.id.et_count);
        mOldPrice = view.findViewById(R.id.tv_old_price);
        mPrice = view.findViewById(R.id.tv_price);
        mDescribe = view.findViewById(R.id.tv_describe);
        mName_Product = view.findViewById(R.id.tv_name_product);
        mImageProduct = view.findViewById(R.id.iv_id_image);
        mBack = view.findViewById(R.id.ib_back_image);
        mContinue = view.findViewById(R.id.ib_continue_image);
        mInStock = view.findViewById(R.id.tv_in_stock);
        mProduct = (Product) getArguments().getSerializable(PRODUCT_KEY);

        if (mProduct.getImageUrl().size() > 0)
            Glide.with(getActivity())
                    .load(mProduct.getImageUrl().get(position))
                    .into(mImageProduct);

        mBack.setVisibility(View.INVISIBLE);
        if (position == mProduct.getImageUrl().size() - 1) mContinue.setVisibility(View.INVISIBLE);

        if (mProduct.isStock()) {
            mInStock.setVisibility(View.GONE);
            mOldPrice.setAlpha(1.0f);
            mPrice.setAlpha(1.0f);
        } else {
            mInStock.setVisibility(View.VISIBLE);
            mOldPrice.setAlpha(0.5f);
            mPrice.setAlpha(0.5f);
        }
        mDescribe.setText(mProduct.getDescription());
        mPrice.setText("₴".concat(mProduct.getProductPrice()));
        mOldPrice.setText("₴".concat(mProduct.getProductOldPrice()));
        mOldPrice.setPaintFlags(mOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        mName_Product.setText(mProduct.getProductName());
        mDescribe.setText(mProduct.getDescription());

        MainActivity activity = (MainActivity) getActivity();
        basket = view.findViewById(R.id.iv_basket);
        circle = view.findViewById(R.id.iv_circle);
        basket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.access)
                    activity.onCreateMyOrdersFragment();
                else activity.onCreateEnterFragment();
            }
        });
        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.access)
                    activity.onCreateMyOrdersFragment();
                else activity.onCreateEnterFragment();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
        if (mProduct.isStock())
            addBasket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.onBackPressed();
                    int countProduct = Integer.parseInt(mCount.getText().toString());
                    if (countProduct > 0) {
                        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt(String.valueOf(mProduct.getProductId()), countProduct);
                        editor.apply();
                        basketSize = activity.getBasketSize();
                        if (basketSize > 0 && circle != null) {
                            circle.setVisibility(View.VISIBLE);
                            circle.setText(String.valueOf(basketSize));
                        }
                    }
                    Toast.makeText(getActivity(), "Ваш товар добавлен в корзину", Toast.LENGTH_LONG).show();
                }
            });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position--;
                if (mProduct.getImageUrl().size() > 0 && position >= 0)
                    Glide.with(getActivity())
                            .load(mProduct.getImageUrl().get(position))
                            .into(mImageProduct);
                if (position == 0) mBack.setVisibility(View.INVISIBLE);
                mContinue.setVisibility(View.VISIBLE);
            }
        });
        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position++;
                if (mProduct.getImageUrl().size() > 0 && position < mProduct.getImageUrl().size())
                    Glide.with(getActivity())
                            .load(mProduct.getImageUrl().get(position))
                            .into(mImageProduct);
                if (position > 0) mBack.setVisibility(View.VISIBLE);
                if (position == mProduct.getImageUrl().size() - 1)
                    mContinue.setVisibility(View.INVISIBLE);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity activity = (MainActivity) getActivity();
        basketSize = activity.getBasketSize();
        if (basketSize > 0 && circle != null) {
            circle.setVisibility(View.VISIBLE);
            circle.setText(String.valueOf(basketSize));
        }
    }

}