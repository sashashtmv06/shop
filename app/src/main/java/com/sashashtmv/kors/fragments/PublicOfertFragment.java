package com.sashashtmv.kors.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sashashtmv.kors.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PublicOfertFragment extends Fragment {


    private static Context mContext;

    public PublicOfertFragment() {
        // Required empty public constructor
    }

    public static PublicOfertFragment newInstance(Context context) {
        PublicOfertFragment fragment = new PublicOfertFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_public_ofert, container, false);
    }

}
