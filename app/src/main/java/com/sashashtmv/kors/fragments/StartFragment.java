package com.sashashtmv.kors.fragments;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;


import com.google.android.material.navigation.NavigationView;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.adapters.CategoriesAdapter;
import com.sashashtmv.kors.model.impl.Category;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Fragment;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.List;


public class StartFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener, IOnBackPressed {
    private static final String CATEGORIES_KEY = "key categories";
    private Callbacks mCallbacks;
    private ImageView facebook;
    private ImageView instagram;
    private ImageView youtube;

    private Button basket;
    private Button circle;
    private View view;
    private int basketSize = 0;
    protected RecyclerView mRecyclerView;
    private CategoriesAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    List<Category> mListCategories;


    public StartFragment() {
        // Required empty public constructor
    }

    public interface Callbacks {
        void getBalance();

        void onCreateAllShopsFragment(String title);

        void onCreatePrivatePoliticFragment();

        void onCreateDeliveryFragment();

        void onCreatePublicOfertFragment();

        void onCreateAboutUsFragment();

        void onSupportFragment();

        void onCreateProfileFragment(String parole);

        void onCreateMyOrdersFragment();

        void onCreateChangePasswordFragment();

        void onCreateEditDataFragment();

        void onCreateWithdrawalOfFundsFragment();

        void onCreateEnterFragment();

        void onCreateReviewsFragment();

        void onCreateContactsFragment();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    public static StartFragment newInstance(List<Category> list) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CATEGORIES_KEY, (Serializable) list);
        StartFragment fragment = new StartFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_start, container, false);
        mCallbacks.getBalance();
        setHasOptionsMenu(true);
        if (getArguments() != null)
            mListCategories = (List<Category>) getArguments().getSerializable(CATEGORIES_KEY);
        mAdapter = new CategoriesAdapter(getActivity(), mListCategories, mCallbacks);
        mRecyclerView = view.findViewById(R.id.rv_category);
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        final DrawerLayout drawer = (DrawerLayout) view.findViewById(R.id.activity_main);
        ImageButton menuRight = (ImageButton) view.findViewById(R.id.menuRight);

        facebook = view.findViewById(R.id.iv_facebook);
        instagram = view.findViewById(R.id.iv_instagram);
        youtube = view.findViewById(R.id.iv_youtube);

        facebook.setOnClickListener(view1 -> {
            String url = "https://www.facebook.com/kors.kraft/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });

        instagram.setOnClickListener(view1 -> {
            String url = "https://www.instagram.com/korskraft/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });

        youtube.setOnClickListener(view1 -> {
            String url = "https://www.youtube.com/channel/UClrU_GNSJ9rQW8ZqRm42kqw";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });

        menuRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });

        basket = view.findViewById(R.id.iv_basket);
        circle = view.findViewById(R.id.iv_circle);
        basket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.access)
                    mCallbacks.onCreateMyOrdersFragment();
                else mCallbacks.onCreateEnterFragment();
            }
        });
        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.access)
                    mCallbacks.onCreateMyOrdersFragment();
                else mCallbacks.onCreateEnterFragment();
            }
        });

        NavigationView navigationView2 = (NavigationView) view.findViewById(R.id.nv_right);
        navigationView2.setNavigationItemSelectedListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity activity = (MainActivity) getActivity();
        basketSize = activity.getBasketSize();
        if (basketSize > 0 && circle != null) {
            circle.setVisibility(View.VISIBLE);
            circle.setText(String.valueOf(basketSize));
        }
    }

    @Override
    public boolean onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) view.findViewById(R.id.activity_main);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return true;
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String text = "";
        if (id == R.id.delivery) {
            mCallbacks.onCreateDeliveryFragment();
        } else if (id == R.id.private_politic) {
            mCallbacks.onCreatePrivatePoliticFragment();
        } else if (id == R.id.public_ofert) {
            mCallbacks.onCreatePublicOfertFragment();
        } else if (id == R.id.about_us) {
            mCallbacks.onCreateAboutUsFragment();
        } else if (id == R.id.my_basket) {
            if (MainActivity.access)
                mCallbacks.onCreateMyOrdersFragment();
            else mCallbacks.onCreateEnterFragment();

        }
        else if (id == R.id.nav_contacts) {
            mCallbacks.onCreateContactsFragment();
        }
        else if (id == R.id.nav_profile) {
            if (MainActivity.access)
                mCallbacks.onCreateProfileFragment(null);
            else mCallbacks.onCreateEnterFragment();
        }
        DrawerLayout drawer = (DrawerLayout) view.findViewById(R.id.activity_main);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

}
