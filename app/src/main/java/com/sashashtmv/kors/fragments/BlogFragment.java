package com.sashashtmv.kors.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sashashtmv.kors.R;
import com.sashashtmv.kors.adapters.ArticlesAdapter;
import com.sashashtmv.kors.model.Article;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlogFragment extends Fragment {

    private static ArticlesAdapter mAdapter;
    private static List<Article> mList;
    private  static Context mContext;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    TextView tvTitle;


    public BlogFragment() {
        // Required empty public constructor
    }

    public static BlogFragment newInstance(Context context, List<Article> list) {
        BlogFragment fragment = new BlogFragment();
        mList = list;
        mContext = context;
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blog, container, false);

        mAdapter = new ArticlesAdapter(mContext, mList);
        mRecyclerView = view.findViewById(R.id.rvArticleList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

}
