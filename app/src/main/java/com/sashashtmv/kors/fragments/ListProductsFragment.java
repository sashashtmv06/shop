package com.sashashtmv.kors.fragments;


import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.adapters.ProductListAdapter;
import com.sashashtmv.kors.model.Product;

import java.util.ArrayList;
import java.util.List;


public class ListProductsFragment extends Fragment {

    static String mTitle;

    private static ProductListAdapter mAdapter;
    private static List<Product> mList;
    private SearchView mSearchView;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    TextView tvTitle;
    Toolbar toolbar;
    ImageButton back;


    public ListProductsFragment() {
        // Required empty public constructor
    }

    public interface OnCartProductFragmentListener {

        void onCreateCartProductFragment(Product product);
    }

    public static ListProductsFragment newInstance(String title, List<Product> list) {
        ListProductsFragment fragment = new ListProductsFragment();
        mTitle = title;
        mList = list;
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_products, container, false);
        // Inflate the layout for this fragment
        toolbar = view.findViewById(R.id.toolbar);
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (toolbar != null) {
            toolbar.setTitle(R.string.title);
            toolbar.setTitleTextColor(getResources().getColor(R.color.design_default_color_primary_dark));

            activity.setSupportActionBar(toolbar);
            activity.getSupportActionBar().setTitle("My title");
        }
        mAdapter = new ProductListAdapter(getActivity(), mList);
        mSearchView = view.findViewById(R.id.search_view);
        tvTitle = view.findViewById(R.id.tv_title);
        back = view.findViewById(R.id.ib_back);
        MainActivity mainActivity = (MainActivity) activity;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.onBackPressed();
            }
        });
        tvTitle.setText(mTitle);

        mRecyclerView = view.findViewById(R.id.rvProductList);
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                findOrder(newText);

                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                tvTitle.setText(mTitle);
                return false;
            }
        });
        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvTitle.setText("");

            }

        });
        return view;
    }

    public void findOrder(String title) {
        //будем удалять все элементы из списка заказов
        //checkAdapter();
        if (mAdapter != null && mList != null) {
            mAdapter.removeAllItems();
            List<Product> products = new ArrayList<>();
            for (int i = 0; i < mList.size(); i++) {
                if (mList.get(i).getProductName().toLowerCase().contains(title.toLowerCase())) {
                    products.add(mList.get(i));
                }
                mAdapter.addShops(products);

            }
        }
    }
}
