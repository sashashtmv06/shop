package com.sashashtmv.kors.fragments;

public interface IOnBackPressed {
    boolean onBackPressed();
}
