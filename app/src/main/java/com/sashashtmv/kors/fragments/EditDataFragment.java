package com.sashashtmv.kors.fragments;


import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.StringRes;

import com.google.gson.Gson;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.model.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sashashtmv.kors.fragments.ValidationFragment.hideKeyboard;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditDataFragment extends Fragment {


    private static Context mContext;
    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;
    private Button mSave;
    private OnFragmentEditDataListener mListener;
    private EditText mTelephone;
    private EditText mPassword;
    private EditText mName;
    private EditText mSurname;
    private EditText mPatronymic;
    private EditText mEmail;


    public EditDataFragment() {
        // Required empty public constructor
    }

    public static EditDataFragment newInstance(Context context) {

        EditDataFragment fragment = new EditDataFragment();

        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_edit_data, container, false);

        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mSave = view.findViewById(R.id.bt_save);
        mName = view.findViewById(R.id.et_name);
        mPassword = view.findViewById(R.id.et_parole);
        mTelephone = view.findViewById(R.id.et_telephone);
        mSurname = view.findViewById(R.id.et_surname);
        mPatronymic = view.findViewById(R.id.et_patronymic);
        mEmail = view.findViewById(R.id.et_email);

        mSave.setOnClickListener(mOnEnterClickListener);

        mTelephone.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursorComplement = s.length() - mTelephone.getSelectionStart();
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");
                if (!editedFlag) {
                    if (phone.length() >= 5 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5);
                        if (phone.length() >= 10)
                            ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5, 8) + "-" + phone.substring(8, 10) + "-" + phone.substring(10);
                        else if (phone.length() >= 8)
                            ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5, 8) + "-" + phone.substring(8);
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    } else if (phone.length() > 2 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2);
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    } else if (phone.length() <= 2) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(";
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    }
                } else {
                    editedFlag = false;
                }
            }
        });


        return view;
    }

    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (checkValidation()) {
                if (EnterFragment.CommonMethod.isNetworkAvailable(mContext))
                    editUserData(mPassword.getText().toString(), mEmail.getText().toString(), "+".concat(mTelephone.getText().toString().replaceAll("[^\\d]", "")), mName.getText().toString(), mSurname.getText().toString(), mPatronymic.getText().toString());
                else
                    EnterFragment.CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
            } else
                showMessage(R.string.login_input_error);
        }
    };

    private void showMessage(@StringRes int string) {
        Toast.makeText(mContext, string, Toast.LENGTH_LONG).show();
    }

    private void editUserData(String parole, String email, String phone, String name, String surname, String patronymic) {
        try {
            String token = mPreferenceHelper.getString("access_token");
            Call<Object> call1 = apiInterface.updateInfoUser("Bearer " + token, parole, email, phone, name, surname, patronymic);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            if (name != null && name.length() > 0)
                                ProfileFragment.mUser.setName(name);
                            if (phone != null && phone.length() > 0)
                                ProfileFragment.mUser.setTelephone(phone);
                            if (surname != null && surname.length() > 0)
                                ProfileFragment.mUser.setSurname(surname);
                            if (patronymic != null && patronymic.length() > 0)
                                ProfileFragment.mUser.setPatronymic(patronymic);
                            if (email != null && email.length() > 0)
                                ProfileFragment.mUser.setMail(email);
                            onButtonPressed(parole);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkValidation() {

        if (mPassword.getText().toString().trim().equals("")) {
            EnterFragment.CommonMethod.showAlert("Пароль нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mTelephone.getText().toString().trim().equals("")) {
            EnterFragment.CommonMethod.showAlert("Телефон нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        }
        return true;
    }

    public void onButtonPressed(String parole) {
        if (mListener != null) {
            mListener.onCreateProfileFragment(parole);
            hideKeyboard(getActivity());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EnterFragment.OnFragmentEnterListener) {
            mListener = (OnFragmentEditDataListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentRegistrationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack(fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount()-2).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    public interface OnFragmentEditDataListener {
        void onCreateProfileFragment(String parole);
    }

}
