package com.sashashtmv.kors.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.model.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ValidationFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener {

    private EditText mTelephone;
    private EditText mPassword;
    private Button mNext;
    private TextView recoveryPassword;

    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;
    static Context mContext;

    private OnFragmentValidationListener mListener;

    public ValidationFragment() {
        // Required empty public constructor
    }


    public static ValidationFragment newInstance(Context context) {
        ValidationFragment fragment = new ValidationFragment();
        mContext = context;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_validation, container, false);

        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();

        mTelephone = view.findViewById(R.id.et_telephone);
        mPassword = view.findViewById(R.id.et_parole);
        mNext = view.findViewById(R.id.bt_next);
        recoveryPassword = view.findViewById(R.id.tv_recovery);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        mNext.setOnClickListener(mOnEnterClickListener);
        recoveryPassword.setOnClickListener(mOnRecoveryListener);

        mTelephone.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursorComplement = s.length() - mTelephone.getSelectionStart();
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");
                if (!editedFlag) {
                    if (phone.length() >= 5 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5);
                        if (phone.length() >= 10)
                            ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5, 8) + "-" + phone.substring(8, 10) + "-" + phone.substring(10);
                        else if (phone.length() >= 8)
                            ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5, 8) + "-" + phone.substring(8);
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    } else if (phone.length() > 2 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2);
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    } else if (phone.length() <= 2) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(";
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    }
                } else {
                    editedFlag = false;
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onFragmentValidation();
            if (checkValidation()) {
                if (EnterFragment.CommonMethod.isNetworkAvailable(mContext))
                    loginRetrofit2Api("+".concat(mTelephone.getText().toString().replaceAll("[^\\d]", "")), mPassword.getText().toString());
                else
                    EnterFragment.CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
            } else
                showMessage(R.string.login_input_error);
        }
    };

    private View.OnClickListener mOnEnterWithoutParoleListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onButtonPressed();
        }
    };

    private View.OnClickListener mOnRecoveryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.onRecoveryPasswordFragment();
        }
    };

    private void showMessage(@StringRes int string) {
        Toast.makeText(mContext, string, Toast.LENGTH_LONG).show();
    }

    private void loginRetrofit2Api(String phone, String code) {
        List<String> list = new ArrayList<>();
        list.add(phone);
        list.add(code);

        try {

            JSONObject paramObject = new JSONObject();
            paramObject.put("username", phone);
            paramObject.put("password", code);

            Call<Object> call1 = apiInterface.createLogin(paramObject.toString());
            call1.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            String token = object.getString("token");
                            if (token != null && token.length() > 0) {
                                mPreferenceHelper.putString("access_token", token);
                                mListener.getOrdersRetrofit2Api();
                                mListener.getUserInfo(code);
                                MainActivity.access = true;
                                onButtonPressed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(mContext, "Введенные данные не верны", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {

        }
    }


    public boolean checkValidation() {
        if (mPassword.getText().toString().trim().equals("")) {
            EnterFragment.CommonMethod.showAlert("Пароль нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mTelephone.getText().toString().trim().equals("")) {
            EnterFragment.CommonMethod.showAlert("Телефон нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        }
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentStart();
            hideKeyboard(getActivity());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EnterFragment.OnFragmentEnterListener) {
            mListener = (OnFragmentValidationListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentRegistrationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentValidationListener {
        void onFragmentStart();

        void onFragmentValidation();

        void onRecoveryPasswordFragment();

        void getOrdersRetrofit2Api();

        void getUserInfo(String parole);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
