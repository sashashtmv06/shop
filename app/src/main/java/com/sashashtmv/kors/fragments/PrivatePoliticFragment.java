package com.sashashtmv.kors.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sashashtmv.kors.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivatePoliticFragment extends Fragment {
    static Context mContext;


    public PrivatePoliticFragment() {
        // Required empty public constructor
    }

    public static PrivatePoliticFragment newInstance(Context context) {
        mContext = context;
        PrivatePoliticFragment fragment = new PrivatePoliticFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_private_politic, container, false);
    }

}
