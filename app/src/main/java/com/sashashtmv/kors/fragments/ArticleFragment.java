package com.sashashtmv.kors.fragments;


import android.os.Bundle;

import android.app.Fragment;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sashashtmv.kors.R;

import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArticleFragment extends Fragment {


    private static URL mImage;
    private static String mTitle;
    private static String mText;

    ImageView imageView;
    TextView tvTitle;
    TextView tvText;

    public ArticleFragment() {
        // Required empty public constructor
    }

    public static ArticleFragment newInstance(URL image, String title, String text) {
        ArticleFragment fragment = new ArticleFragment();

        mImage = image;
        mTitle = title;
        mText = text;

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);

        imageView = view.findViewById(R.id.iv_image);
        tvTitle = view.findViewById(R.id.tv_title_article);
        tvText = view.findViewById(R.id.tv_text_article);
        tvText.setMovementMethod(new ScrollingMovementMethod());

        Glide.with(getActivity())
                .load(String.valueOf(mImage))
                .into(imageView);

        tvTitle.setText(mTitle);
        tvText.setText(mText);

        return view;
    }

}
