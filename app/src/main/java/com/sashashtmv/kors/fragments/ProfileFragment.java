package com.sashashtmv.kors.fragments;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.adapters.OrdersAdapter;
import com.sashashtmv.kors.adapters.OrdersInProfileAdapter;
import com.sashashtmv.kors.model.Order;
import com.sashashtmv.kors.model.Product;
import com.sashashtmv.kors.model.User;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    private static Context mContext;
    private static List<Order> mList;
    protected RecyclerView mRecyclerView;
    private static OrdersInProfileAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    static User mUser;
    private Button edit;

    private TextView changePassword;
    private TextView mName;
    private TextView mMail;
    private String mParole;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(List<Order> list, Context context, User user, String parole) {
        Bundle bundle = new Bundle();
        bundle.putString("parole", parole);
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(bundle);
        mContext = context;
        mList = list;
        mUser = user;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        mParole = getArguments().getString("parole");
        mAdapter = new OrdersInProfileAdapter(getActivity(), mList);
        mRecyclerView = view.findViewById(R.id.rv_my_orders);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        edit = view.findViewById(R.id.bt_edit);
        changePassword = view.findViewById(R.id.tv_change_password);
        mName = view.findViewById(R.id.tv_name);
        mMail = view.findViewById(R.id.tv_mail);
        mName.setText(mUser.getName().concat(" ").concat(mUser.getSurname()));
        mMail.setText(mUser.getMail());
        changePassword.setText(mParole);

        final MainActivity activity = (MainActivity) mContext;

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onCreateEditDataFragment();
            }
        });

        return view;
    }
}
