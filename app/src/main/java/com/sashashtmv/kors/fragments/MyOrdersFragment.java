package com.sashashtmv.kors.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.adapters.OrdersAdapter;
import com.sashashtmv.kors.model.Product;

import java.util.List;


public class MyOrdersFragment extends Fragment {


    private static OrdersAdapter mAdapter;
    private static List<Product> mList;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    ImageButton back;
    TextView sum;
    private SharedPreferences sharedPref;
    LinearLayout completeOrder;

    public MyOrdersFragment() {
        // Required empty public constructor
    }

    public static MyOrdersFragment newInstance(List<Product> list, Context context) {
        MyOrdersFragment fragment = new MyOrdersFragment();
        mList = list;
        return fragment;
    }

    public interface OnCompleteOrderFragmentListener {

        void onCreateCompleteOrderFragment(List<Product> list);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_orders, container, false);

        mAdapter = new OrdersAdapter(getActivity(), mList);
        back = view.findViewById(R.id.ib_back);
        sum = view.findViewById(R.id.tv_sum_value);
        completeOrder = view.findViewById(R.id.ll_header);
        sharedPref = ((MainActivity) getActivity()).getPreferences(Context.MODE_PRIVATE);
        MainActivity activity = (MainActivity)getActivity();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
        completeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mList.size() > 0) {
                    MainActivity activity = (MainActivity) getActivity();
                    activity.onCreateCompleteOrderFragment(mList);
                }
            }
        });

        mRecyclerView = view.findViewById(R.id.rv_my_orders);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        updateUi();
        mAdapter.setOnDataChangeListener(new OrdersAdapter.OnDataChangeListener(){
            public void onDataChanged(){
                updateUi();
            }
        });
        return view;
    }

    private void updateUi() {
        int total = 0;
        for(Product product : mList){
            int count = sharedPref.getInt(String.valueOf(product.getProductId()), 0);
            if(count > 0){
                total += count * Double.parseDouble(product.getProductPrice());
            }
        }
        sum.setText("₴".concat(String.valueOf(total)));
    }

}
