package com.sashashtmv.kors.fragments;


import android.app.Activity;
import android.content.Context;

import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;

import android.app.Fragment;

import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.FragmentManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordRecoveryFragment extends Fragment {


    private static Context mContext;
    private EditText mTelephone;
    private APIInterface apiInterface;
    private Button sendPhone;
    static FragmentManager mFragmentManager;

    public PasswordRecoveryFragment() {
        // Required empty public constructor
    }

    public static PasswordRecoveryFragment newInstance(Context context) {
        PasswordRecoveryFragment fragment = new PasswordRecoveryFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password_recovery, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        mTelephone = view.findViewById(R.id.et_phone);
        sendPhone = view.findViewById(R.id.bt_next);
        mFragmentManager = ((MainActivity) mContext).getFragmentManager();

        sendPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed();
            }
        });

        mTelephone.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            private boolean backspacingFlag = false;
            private boolean editedFlag = false;
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursorComplement = s.length() - mTelephone.getSelectionStart();
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");
                if (!editedFlag) {
                    if (phone.length() >= 5 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5);
                        if (phone.length() >= 10)
                            ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5, 8) + "-" + phone.substring(8, 10) + "-" + phone.substring(10);
                        else if (phone.length() >= 8)
                            ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2, 5) + ")" + phone.substring(5, 8) + "-" + phone.substring(8);
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    } else if (phone.length() > 2 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(" + phone.substring(2);
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    } else if (phone.length() <= 2) {
                        editedFlag = true;
                        String ans = "+" + phone.substring(0, 2) + "(";
                        mTelephone.setText(ans);
                        mTelephone.setSelection(mTelephone.getText().length() - cursorComplement);
                    }
                } else {
                    editedFlag = false;
                }
            }
        });

        return view;
    }

    private void loginRetrofit2Api(String phone) {
        try {
            Call<Object> call1 = apiInterface.resetPassword(phone);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Введенный номер отсутствует в нашей базе", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    } else
                        Toast.makeText(getActivity(), "Введенный номер отсутствует в нашей базе", Toast.LENGTH_LONG).show();

                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {

        }
    }


    public boolean checkValidation() {

        if (mTelephone.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Введите адрес своей почты", (MainActivity) mContext);
            return false;
        }
        return true;
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Fragment fragment = mFragmentManager.findFragmentByTag("enterfragment");
                            if (fragment != null) {
                                fragment.getFragmentManager().beginTransaction().replace(R.id.container, fragment)
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                    });
            try {
                AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (checkValidation() && CommonMethod.isNetworkAvailable(mContext)) {
            loginRetrofit2Api("+".concat(mTelephone.getText().toString().replaceAll("[^\\d]", "")));
        } else CommonMethod.showAlert("Подключите интернет", (MainActivity) mContext);
    }

}
