package com.sashashtmv.kors.fragments;


import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sashashtmv.kors.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeliveryFragment extends Fragment {


    private static Context mContext;

    public DeliveryFragment() {
        // Required empty public constructor
    }
    public static DeliveryFragment newInstance(Context context) {
        mContext = context;
        DeliveryFragment fragment = new DeliveryFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delivery, container, false);
    }

}
