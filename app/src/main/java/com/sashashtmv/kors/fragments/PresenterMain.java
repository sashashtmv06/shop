package com.sashashtmv.kors.fragments;

import com.sashashtmv.kors.model.AreaModel;
import com.sashashtmv.kors.model.CityModel;
import com.sashashtmv.kors.model.PunktModel;
import com.sashashtmv.kors.model.impl.InteractorData;
import com.sashashtmv.kors.model.impl.InteractorDataImpl;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class PresenterMain implements ContractMain.IPresenerMain {
    private InteractorData mInteractorData;
    private ContractMain.IViewMain view;

    public PresenterMain(ContractMain.IViewMain IView) {
        this.view = IView;
        mInteractorData = new InteractorDataImpl();
    }

    @Override
    public void getAreas() {
        mInteractorData.getAreas()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<AreaModel>>() {
                    @Override
                    public void accept(List<AreaModel> areaModels) throws Exception {
                        if (areaModels == null) {
                            view.showError();
                        }

                        view.setAreas(areaModels);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        view.showError();
                    }
                });
    }

    @Override
    public void getCities(String nameArea) {
        mInteractorData.getCities(nameArea)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<CityModel>>() {
                    @Override
                    public void accept(List<CityModel> cityModels) throws Exception {
                        if (cityModels == null) {
                            view.showError();
                        }
                        view.setCities(cityModels);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        view.showError();
                    }
                });

    }

    @Override
    public void getPunkt(String nameCity) {
        mInteractorData.getPunkts(nameCity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<PunktModel>>() {
                    @Override
                    public void accept(List<PunktModel> punktModels) throws Exception {
                        if (punktModels == null) {
                            view.showError();
                        }
                        view.setPunkts(punktModels);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        view.showError();
                    }
                });
    }
}
