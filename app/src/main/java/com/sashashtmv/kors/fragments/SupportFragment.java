package com.sashashtmv.kors.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.model.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends Fragment {


    private static Context mContext;
    private EditText name;
    private EditText mail;
    private EditText question;
    private Button sendQuestion;
    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;

    public SupportFragment() {
        // Required empty public constructor
    }

    public static SupportFragment newInstance(Context context) {
        SupportFragment fragment = new SupportFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_support, container, false);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();

        name = view.findViewById(R.id.et_name);
        mail = view.findViewById(R.id.et_patronymic);
        question = view.findViewById(R.id.et_message);
        sendQuestion = view.findViewById(R.id.bt_send);

        sendQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed();
                ((MainActivity) mContext).onBackPressed();
            }
        });


        return view;
    }

    private void sendQuestion(String text) {
        try {
            String token = mPreferenceHelper.getString("access_token");
            Call<Object> call1 = apiInterface.sendQuestion("Bearer " + token, text);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null && response.code() == 0) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            try {
                AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public boolean checkValidation() {

        if (name.getText().toString().equals("") || mail.getText().toString().equals("")) {
            SupportFragment.CommonMethod.showAlert("Заполните все поля", (MainActivity) mContext);
            return false;
        }
        if (question.getText().toString().equals("")) {
            SupportFragment.CommonMethod.showAlert("Напишите Ваш вопрос", (MainActivity) mContext);
            return false;
        }
        return true;
    }


    public void onButtonPressed() {
        if (!SupportFragment.CommonMethod.isNetworkAvailable(mContext)) {
            SupportFragment.CommonMethod.showAlert("Подключите интернет", (MainActivity) mContext);
        } else if (checkValidation()) {
            sendQuestion(name.getText() + "\n" + mail.getText() + "\n" + question.getText());
            SupportFragment.CommonMethod.showAlert("Ваш вопрос направлен в службу поддержки.", (MainActivity) mContext);
        }
    }

}
