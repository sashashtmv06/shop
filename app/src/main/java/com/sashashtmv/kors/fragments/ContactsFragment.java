package com.sashashtmv.kors.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.model.Product;

import java.io.Serializable;

import static android.content.Intent.ACTION_SENDTO;
import static android.content.Intent.EXTRA_EMAIL;
import static android.content.Intent.EXTRA_SUBJECT;


public class ContactsFragment extends Fragment {

    private TextView text1;
    private TextView text2;
    private TextView text3;


    public ContactsFragment() {
        // Required empty public constructor
    }


    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        text1 = view.findViewById(R.id.tv_box1);
        text2 = view.findViewById(R.id.tv_box2);
        text3 = view.findViewById(R.id.tv_box3);
        final String str1 = "samogon.kors@gmail.com";
        final String str2 = "aleksandr.kors.company@gmail.com";
        final String str3 = "+38 (096) 99-00-700";
        final String str4 = "+38 (073) 99-00-700";
        final String str5 = "+38 (066) 99-00-700";
        final String str6 = "+38 (097) 15-90-247";
        final String str7 = "+38 (063) 24-45-576";
        ClickSpan.clickify(text1, str1,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + str1));
                startActivity(Intent.createChooser(emailIntent, "Send feedback"));
            }
        });
        ClickSpan.clickify(text2, str1,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + str1));
                startActivity(Intent.createChooser(emailIntent, "Send feedback"));
            }
        });
        ClickSpan.clickify(text3, str2,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + str2));
                startActivity(Intent.createChooser(emailIntent, "Send feedback"));
            }
        });
        ClickSpan.clickify(text1, str3,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                String phone = str3.replaceAll("[^\\d]", "");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+" +phone));
                startActivity(intent);
            }
        });
        ClickSpan.clickify(text1, str4,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                String phone = str4.replaceAll("[^\\d]", "");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+" +phone));
                startActivity(intent);
            }
        });
        ClickSpan.clickify(text1, str5,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                String phone = str5.replaceAll("[^\\d]", "");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+" +phone));
                startActivity(intent);
            }
        });
        ClickSpan.clickify(text2, str6,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                String phone = str6.replaceAll("[^\\d]", "");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+" +phone));
                startActivity(intent);
            }
        });
        ClickSpan.clickify(text3, str7,new ClickSpan.OnClickListener()
        {
            @Override
            public void onClick() {
                String phone = str7.replaceAll("[^\\d]", "");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+" +phone));
                startActivity(intent);
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    public static class ClickSpan extends ClickableSpan {

        private OnClickListener mListener;

        public ClickSpan(OnClickListener listener) {
            mListener = listener;
        }

        @Override
        public void onClick(View widget) {
            if (mListener != null) mListener.onClick();
        }

        public interface OnClickListener {
            void onClick();
        }
        public static void clickify(TextView view, final String clickableText,
                                    final ClickSpan.OnClickListener listener) {

            CharSequence text = view.getText();
            String string = text.toString();
            ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
            ClickSpan span = new ClickSpan(listener);


            int start = string.indexOf(clickableText);
            int end = start + clickableText.length();
            if (start == -1) return;

            if (text instanceof Spannable) {
                ((Spannable) text).setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                SpannableString s = SpannableString.valueOf(text);
                s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                s.setSpan(foregroundSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                view.setText(s);
            }

            MovementMethod m = view.getMovementMethod();
            if ((m == null) || !(m instanceof LinkMovementMethod)) {
                view.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

}