package com.sashashtmv.kors.fragments;

import android.app.AlertDialog;
import android.content.Context;

import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;

public class WithdrawalOfFundsFragment extends Fragment {


    private static Context mContext;
    private ImageView iv_visa;
    private ImageView iv_mastercard;
    private ImageView iv_vodafone;
    private ImageView iv_kiivstar;
    private ImageView iv_lifecell;
    private ImageView iv_private24;
    MainActivity activity;

    AlertDialog alert;


    public WithdrawalOfFundsFragment() {
        // Required empty public constructor
    }

    public static WithdrawalOfFundsFragment newInstance(Context context) {
        WithdrawalOfFundsFragment fragment = new WithdrawalOfFundsFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_withdrawal_of_funds, container, false);
        activity = (MainActivity) mContext;

        iv_visa = view.findViewById(R.id.iv_visa);
        iv_mastercard = view.findViewById(R.id.iv_mastercard);
        iv_vodafone = view.findViewById(R.id.iv_vodafone);
        iv_kiivstar = view.findViewById(R.id.iv_kiivstar);
        iv_lifecell = view.findViewById(R.id.iv_lifecell);
        iv_private24 = view.findViewById(R.id.iv_private);

        iv_visa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = new AlertDialog.Builder(mContext).create();
                showDialog("Введите номер вашей карты", "Вывод денег на карту Visa", inflater, R.drawable.visa, "card");
            }
        });

        iv_mastercard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = new AlertDialog.Builder(mContext).create();
                showDialog("Введите номер вашей карты", "Вывод денег на карту Mastercard", inflater, R.drawable.master_card, "card");
            }
        });

        iv_private24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = new AlertDialog.Builder(mContext).create();
                showDialog("Введите номер вашей карты", "Вывод денег на карту Private24", inflater, R.drawable.privat24, "card");
            }
        });

        iv_vodafone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = new AlertDialog.Builder(mContext).create();
                showDialog("Введите номер вашего телефона\n(напр.0667930472)", "Вывод денег на номер Vodafone", inflater, R.drawable.vodafone, "vodafone");
            }
        });

        iv_kiivstar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = new AlertDialog.Builder(mContext).create();
                showDialog("Введите номер вашего телефона\n(напр.0677930472)", "Вывод денег на номер Киевстар", inflater, R.drawable.kiivstar, "kyivstar");
            }
        });

        iv_lifecell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = new AlertDialog.Builder(mContext).create();
                showDialog("Введите номер вашего телефона\n(напр.0637930472)", "Вывод денег на номер Киевстар", inflater, R.drawable.lifecel, "kyivstar");
            }
        });


        return view;
    }

    public void showDialog(String message, String title, LayoutInflater inflater, int resIcon, String type) {
        final View view = inflater.inflate(R.layout.show_dialog, null);
        alert.setMessage(message);
        alert.setTitle(title);
        alert.setIcon(resIcon);
        alert.setCancelable(false);

        final EditText etSum = (EditText) view.findViewById(R.id.et_sum);
        final EditText etNumber = (EditText) view.findViewById(R.id.et_number);

        alert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String sum = etSum.getText().toString();
                String number = etNumber.getText().toString();
                if (sum.length() > 0 && Double.parseDouble(sum) >= 4 && checkValidation(sum, number, type)) {
                    activity.withdrawals(sum, type, number);
                    Toast.makeText(mContext, "Ваш запрос на вывод средств обрабатывается", Toast.LENGTH_LONG).show();
                    activity.getBalance();
                    alert.dismiss();
                }
            }
        });

        alert.setView(view);
        alert.show();

    }

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean checkValidation(String sum, String number, String type) {
        if (sum.length() > 0 && Double.parseDouble(sum) < 4) {
            Toast.makeText(mContext, "Введите сумму не менее 4", Toast.LENGTH_LONG).show();
            return false;
        }
        if (number.length() != 10 && (type.equals("kyivstar") || type.equals("vodafone") || type.equals("lifecell"))) {
            Toast.makeText(mContext, "Номер телефона должен состоять из 10 цифр", Toast.LENGTH_LONG).show();
            return false;
        } else if (number.length() != 16 && (type.equals("card"))) {
            Toast.makeText(mContext, "Номер карты должен состоять из 16 цифр", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!isNetworkAvailable(mContext)) {
            Toast.makeText(mContext, "Проверьте подключение к интернету", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}
