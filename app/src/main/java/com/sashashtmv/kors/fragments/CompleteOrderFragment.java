package com.sashashtmv.kors.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.adapters.CompleteOrdersAdapter;
import com.sashashtmv.kors.adapters.CustomSpinnerAdapter;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.model.AreaModel;
import com.sashashtmv.kors.model.CityModel;
import com.sashashtmv.kors.model.PreferenceHelper;
import com.sashashtmv.kors.model.Product;
import com.sashashtmv.kors.model.PunktModel;
import com.sashashtmv.kors.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class CompleteOrderFragment extends Fragment implements ContractMain.IViewMain {
    private static final String LIST_PRODUCT_KEY = "listOrders";
    private PresenterMain mPresenterMain;
    private Spinner areaSpinner;
    private Spinner citySpinner;
    private Spinner punktSpinner;
    private String nameCity;
    private String refArea;
    private String namePunkt;
    private String payment;
    List<Product> listBasketProducts;
    private CompleteOrdersAdapter mAdapter;
    private ImageButton back;
    private TextView sum;
    private TextView describeUser;
    private LinearLayout completeOrder;
    private SharedPreferences sharedPref;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;
    private OnFragmentCompleteOrderListener mListener;
    static User mUser;

    public CompleteOrderFragment() {
    }

    public static CompleteOrderFragment newInstance(List<Product> listBasketProducts, User user) {
        Bundle bundle = new Bundle();
        mUser = user;
        bundle.putSerializable(LIST_PRODUCT_KEY, (Serializable) listBasketProducts);
        CompleteOrderFragment fragment = new CompleteOrderFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenterMain = new PresenterMain(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_complete_order, container, false);
        listBasketProducts = (List<Product> )getArguments().getSerializable(LIST_PRODUCT_KEY);
        areaSpinner = rootView.findViewById(R.id.areas_spinner);
        citySpinner = rootView.findViewById(R.id.city_spinner);
        punktSpinner = rootView.findViewById(R.id.punkt_spinner);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        PreferenceHelper.getInstance().init(getActivity());
        mPreferenceHelper = PreferenceHelper.getInstance();

        mAdapter = new CompleteOrdersAdapter(getActivity(), listBasketProducts);
        back = rootView.findViewById(R.id.ib_back);
        sum = rootView.findViewById(R.id.tv_sum_value);
        describeUser = rootView.findViewById(R.id.tv_describe_user);
        describeUser.setText(mUser.getSurname().concat(" ").concat(mUser.getName().substring(0,1)).concat(". ").concat(mUser.getPatronymic().substring(0,1)).concat("."));
        completeOrder = rootView.findViewById(R.id.ll_header);
        sharedPref = ((MainActivity) getActivity()).getPreferences(Context.MODE_PRIVATE);

        MainActivity activity = (MainActivity)getActivity();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });

        final Spinner paymentSpinner = rootView.findViewById(R.id.payment_spinner);
        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(getActivity(), R.array.payments, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paymentSpinner.setAdapter(adapter);
        paymentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View itemSelected, int selectedItemPosition, long selectedId) {
                String[] choose = getResources().getStringArray(R.array.payments);
                payment = choose[selectedItemPosition];
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        mPresenterMain.getAreas();

        mRecyclerView = rootView.findViewById(R.id.rv_my_orders);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        updateUi();

        completeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendNewOrder();
            }
        });

        return rootView;
    }

    private void sendNewOrder() {

        try {
            JSONArray array = new JSONArray();
            for (int i = 0; i < listBasketProducts.size(); i++) {
                JSONObject orderObject = new JSONObject();
                int count = sharedPref.getInt(String.valueOf(listBasketProducts.get(i).getProductId()), 0);
                orderObject.put("id", listBasketProducts.get(i).getProductId());
                orderObject.put("amount", count);
                array.put(orderObject);
            }

            JSONObject paramObject = new JSONObject();
            paramObject.put("products", array);
            paramObject.put("delivery_address", refArea.concat(", ").concat(nameCity).concat(", ").concat(namePunkt));
            paramObject.put("wishes", "");
            String token = mPreferenceHelper.getString("access_token");
            Call<Object> call1 = apiInterface.sendOrder("Bearer " + token, paramObject.toString());

            call1.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {

                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            boolean access = object.getBoolean("status");

                            if (access) {
                               mListener.onClearOrder();
                                mListener.onFragmentStart();
                                Toast.makeText(getActivity(), "Спасибо, Ваш заказ принят", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Введенные данные не верны", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                }
            });
        } catch (Exception e) {

        }
    }

    private void updateUi() {
        int total = 0;
        for(Product product : listBasketProducts){
            int count = sharedPref.getInt(String.valueOf(product.getProductId()), 0);
            if(count > 0){
                total += count * Double.parseDouble(product.getProductPrice());
            }
        }
        sum.setText("₴".concat(String.valueOf(total)));
    }

    @Override
    public void setPunkts(List<PunktModel> list) {
        List<String> temp = new ArrayList<>();
        for (PunktModel model : list) {
            temp.add(model.getDescription());
        }
        final String[] areas = temp.toArray(new String[0]);
        punktSpinner.setAdapter(new CustomSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, areas));
        punktSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                namePunkt = areas[selectedItemPosition];
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void setAreas(List<AreaModel> list) {
        List<String> temp = new ArrayList<>();
        List<String> tempRef = new ArrayList<>();
        for (AreaModel model : list) {
            temp.add(model.getDescription());
            tempRef.add(model.getRef());
        }
        final String[] areas = temp.toArray(new String[0]);
        final String[] refAreas = tempRef.toArray(new String[0]);
        areaSpinner.setAdapter(new CustomSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, areas));
        areaSpinner.setSelection(1);
        areaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                refArea = refAreas[selectedItemPosition];
                mPresenterMain.getCities(refArea);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public void setCities(List<CityModel> list) {
        List<String> temp = new ArrayList<>();
        for (CityModel model : list) {
            temp.add(model.getDescription());
        }
        final String[] areas = temp.toArray(new String[0]);
        citySpinner.setAdapter(new CustomSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, areas));
        citySpinner.setSelection(1);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                nameCity = areas[selectedItemPosition];
                mPresenterMain.getPunkt(nameCity);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentCompleteOrderListener) {
            mListener = (OnFragmentCompleteOrderListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentRegistrationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentCompleteOrderListener {
        void onClearOrder();
        void onFragmentStart();
    }

    @Override
    public void showError() {
        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
    }
}
