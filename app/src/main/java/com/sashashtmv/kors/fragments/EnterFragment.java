package com.sashashtmv.kors.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.facebook.CallbackManager;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.api.APIClient;
import com.sashashtmv.kors.api.APIInterface;
import com.sashashtmv.kors.model.PreferenceHelper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import android.app.Fragment;

public class EnterFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener {

    private Button mNext;
    private Button mRegistration;

    private APIInterface apiInterface;
    private GoogleSignInOptions mGoogleSignInOptions;
    private PreferenceHelper mPreferenceHelper;

    private OnFragmentEnterListener mListener;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;


    public EnterFragment() {
        // Required empty public constructor
    }


    public static EnterFragment newInstance(Context context) {
        EnterFragment fragment = new EnterFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter, container, false);


        PreferenceHelper.getInstance().init(getActivity());
        mPreferenceHelper = PreferenceHelper.getInstance();
        mNext = view.findViewById(R.id.bt_next);
        mRegistration = view.findViewById(R.id.bt_registration);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        mNext.setOnClickListener(mOnEnterClickListener);
        mRegistration.setOnClickListener(mOnRegistrationListener);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoogleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.ClientID))
                .requestEmail()
                .build();

        try {
            // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder((MainActivity) getActivity())
                        .enableAutoManage((MainActivity) getActivity(), this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, mGoogleSignInOptions)
                        .build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage((MainActivity) getActivity());
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }


    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onFragmentValidation();
        }
    };

    private View.OnClickListener mOnRegistrationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.onFragmentRegistration();
        }
    };

    private View.OnClickListener mOnRecoveryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.onRecoveryPasswordFragment();
        }
    };

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            try {
                builder.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentStart();
            hideKeyboard(getActivity());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentEnterListener) {
            mListener = (OnFragmentEnterListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentRegistrationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentEnterListener {

        void onFragmentStart();

        void onFragmentRegistration();

        void onRecoveryPasswordFragment();

        void onFragmentValidation();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
