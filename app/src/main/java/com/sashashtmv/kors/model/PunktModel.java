package com.sashashtmv.kors.model;

public class PunktModel {
    private String Description;
    private String Phone;
    private String TypeOfWarehouse;
    private String CityDescription;
    private String Number;
    private String Ref;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getRef() {
        return Ref;
    }

    public void setRef(String ref) {
        Ref = ref;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getTypeOfWarehouse() {
        return TypeOfWarehouse;
    }

    public void setTypeOfWarehouse(String typeOfWarehouse) {
        TypeOfWarehouse = typeOfWarehouse;
    }

    public String getCityDescription() {
        return CityDescription;
    }

    public void setCityDescription(String cityDescription) {
        CityDescription = cityDescription;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }
}
