package com.sashashtmv.kors.model.impl;

import com.sashashtmv.kors.model.AreaModel;
import com.sashashtmv.kors.model.CityModel;
import com.sashashtmv.kors.model.PunktModel;

import java.util.List;

import io.reactivex.Single;


public interface InteractorData {

    /**
     * Load Area`s
     */
    Single<List<AreaModel>> getAreas();

    /**
     * Load Cities
     */
    Single<List<CityModel>> getCities(String nameArea);

    /**
     * Load Cities
     */
    Single<List<PunktModel>> getPunkts(String nameCity);
}
