package com.sashashtmv.kors.model;

import java.io.Serializable;
import java.net.URL;
import java.util.List;

public class Product implements Serializable {
    private int productId;
    private String productName;
    private String productPrice;
    private String productOldPrice;
    private boolean isNew;
    private boolean isStock;
    private int sku;
    private double categoryId;
    private String categoryName;
    private String description;
    private boolean isPublished;
    private List<String> imageUrl;


    public Product(int productId, String productName, String productPrice, String productOldPrice, boolean isNew, boolean isStock, int sku, double categoryId, String categoryName,  String description, boolean isPublished, List<String> imageUrl) {
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productOldPrice = productOldPrice;
        this.isNew = isNew;
        this.isStock = isStock;
        this.sku = sku;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.description = description;
        this.isPublished = isPublished;
        this.imageUrl = imageUrl;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public double getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(double categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isPublished() {
        return isPublished;
    }

    public void setPublished(boolean published) {
        this.isPublished = published;
    }

    public String getProductName() {
        return productName;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getImageUrl() {
        return imageUrl;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductOldPrice() {
        return productOldPrice;
    }

    public void setProductOldPrice(String productOldPrice) {
        this.productOldPrice = productOldPrice;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isStock() {
        return isStock;
    }

    public void setStock(boolean stock) {
        isStock = stock;
    }

    public int getSku() {
        return sku;
    }

    public void setSku(int sku) {
        this.sku = sku;
    }
}
