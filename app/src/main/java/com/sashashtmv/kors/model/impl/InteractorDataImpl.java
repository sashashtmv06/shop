package com.sashashtmv.kors.model.impl;

import com.sashashtmv.kors.model.AreaModel;
import com.sashashtmv.kors.model.CityModel;
import com.sashashtmv.kors.model.PunktModel;
import com.sashashtmv.kors.repository.RemoteRepository;
import com.sashashtmv.kors.repository.impl.RemoteRepositoryImpl;
import com.sashashtmv.kors.response.AreaResponse;
import com.sashashtmv.kors.response.CityResponse;
import com.sashashtmv.kors.response.DataAreaResponse;
import com.sashashtmv.kors.response.DataCityResponse;
import com.sashashtmv.kors.response.DataPunktResponse;
import com.sashashtmv.kors.response.PunktResponse;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.functions.Function;

import static com.sashashtmv.kors.converter.ModelConverter.convertToListAreaModel;
import static com.sashashtmv.kors.converter.ModelConverter.convertToListCityModel;
import static com.sashashtmv.kors.converter.ModelConverter.convertToListPunktModel;



public class InteractorDataImpl implements InteractorData {
    private RemoteRepository mRemoteRepository;

    public InteractorDataImpl() {
        mRemoteRepository = RemoteRepositoryImpl.getInstance();
    }

    @Override
    public Single<List<AreaModel>> getAreas() {
        return mRemoteRepository.loadAreas()
                .map(new Function<DataAreaResponse, List<AreaModel>>() {
                    @Override
                    public List<AreaModel> apply(DataAreaResponse dataArea) throws Exception {
                        List<AreaResponse> responseList = dataArea.getAreaResponses();
                        if (responseList != null) {
                            return convertToListAreaModel(responseList);
                        }
                        return null;
                    }
                });
    }

    @Override
    public Single<List<CityModel>> getCities(String nameArea) {
        return mRemoteRepository.loadCities(nameArea)
                .map(new Function<DataCityResponse, List<CityModel>>() {
                    @Override
                    public List<CityModel> apply(DataCityResponse dataCityResponse) throws Exception {
                        List<CityResponse> responseList = dataCityResponse.getCityResponses();
                        if (responseList != null) {
                            return convertToListCityModel(responseList);
                        }
                        return null;
                    }
                });
    }

    @Override
    public Single<List<PunktModel>> getPunkts(String nameCity) {
        return mRemoteRepository.loadPunkts(nameCity)
                .map(new Function<DataPunktResponse, List<PunktModel>>() {
                    @Override
                    public List<PunktModel> apply(DataPunktResponse dataPunktResponse) throws Exception {
                        List<PunktResponse> responseList = dataPunktResponse.getPunktResponses();
                        if (responseList != null) {
                            return convertToListPunktModel(responseList);
                        }
                        return null;
                    }
                });
    }
}
