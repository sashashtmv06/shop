package com.sashashtmv.kors.model;

public class User {
    private String mail;
    private String telephone;
    private String name;
    private String surname;
    private String patronymic;

    public User(String mail, String telephone, String name, String surname, String patronymic) {
        this.mail = mail;
        this.telephone = telephone;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
}
