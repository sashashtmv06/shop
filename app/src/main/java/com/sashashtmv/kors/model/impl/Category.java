package com.sashashtmv.kors.model.impl;

import java.io.Serializable;

public class Category implements Serializable {
   private int id;
    private String name;
    private Boolean published;

    public Category(int id, String name, Boolean published) {
        this.id = id;
        this.name = name;
        this.published = published;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }
}
