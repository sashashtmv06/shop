package com.sashashtmv.kors.model;

public class Order {
    private String date;
    private String id;
    private String storeName;
    private String price;
    private String status;
    private String count;

    public Order(String date, String id, String storeName, String price, String status, String count) {
        this.date = date;
        this.id = id;
        this.storeName = storeName;
        this.price = price;
        this.status = status;
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
