package com.sashashtmv.kors.model;

public class Review {
    private String nameUser;
    private String textReview;
    private String date;

    public Review(String nameUser, String textReview, String date) {
        this.nameUser = nameUser;
        this.textReview = textReview;
        this.date = date;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getTextReview() {
        return textReview;
    }

    public void setTextReview(String textReview) {
        this.textReview = textReview;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
