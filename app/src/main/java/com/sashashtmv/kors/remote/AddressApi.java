package com.sashashtmv.kors.remote;

import com.sashashtmv.kors.request.AreaRequest;
import com.sashashtmv.kors.request.CityRequest;
import com.sashashtmv.kors.request.PunktRequest;
import com.sashashtmv.kors.response.DataAreaResponse;
import com.sashashtmv.kors.response.DataCityResponse;
import com.sashashtmv.kors.response.DataPunktResponse;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * Created by Andrii Papai on 03.12.2017.
 */

public interface AddressApi {

    @POST("json/")
    Single<DataAreaResponse> getAreas(@Body AreaRequest areaRequest);

    @POST("json/")
    Single<DataCityResponse> getCities(@Body CityRequest cityRequest);

    @POST("json/")
    Single<DataPunktResponse> getPunkts(@Body PunktRequest punktRequest);

}
