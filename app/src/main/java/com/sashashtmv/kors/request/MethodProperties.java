package com.sashashtmv.kors.request;

import com.google.gson.annotations.SerializedName;

public class MethodProperties {
    @SerializedName("CityName")
    private String CityName;


    public MethodProperties(String settlementRef) {
        CityName = settlementRef;
    }

    public MethodProperties(){

    }

    public String getSettlementRef() {
        return CityName;
    }

    public void setSettlementRef(String settlementRef) {
        CityName = settlementRef;
    }
}
