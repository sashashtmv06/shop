package com.sashashtmv.kors.request;

import com.google.gson.annotations.SerializedName;

import static com.sashashtmv.kors.AppConstant.API_KEY;


public class PunktRequest {
    @SerializedName("modelName")
    private String modelName;
    @SerializedName("calledMethod")
    private String calledMethod;
    @SerializedName("apiKey")
    private String apiKey;
    @SerializedName("methodProperties")
    private MethodProperties methodProperties;

    public PunktRequest(String nameCity) {
        modelName = "AddressGeneral";
        calledMethod = "getWarehouses";
        methodProperties = new MethodProperties(nameCity);
        apiKey = API_KEY;
    }

    public PunktRequest(String modelName, String calledMethod, String apiKey, MethodProperties methodProperties) {
        this.modelName = modelName;
        this.calledMethod = calledMethod;
        this.methodProperties = methodProperties;
        this.apiKey = apiKey;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getCalledMethod() {
        return calledMethod;
    }

    public void setCalledMethod(String calledMethod) {
        this.calledMethod = calledMethod;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public MethodProperties getMethodProperties() {
        return methodProperties;
    }

    public void setMethodProperties(MethodProperties methodProperties) {
        this.methodProperties = methodProperties;
    }
}
