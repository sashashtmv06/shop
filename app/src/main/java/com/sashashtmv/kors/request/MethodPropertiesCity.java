package com.sashashtmv.kors.request;

import com.google.gson.annotations.SerializedName;

public class MethodPropertiesCity {
    @SerializedName("AreaRef")
    private String AreaRef;


    public MethodPropertiesCity(String areaRef) {
        AreaRef = areaRef;
    }

    public MethodPropertiesCity(){

    }

    public String getSettlementRef() {
        return AreaRef;
    }

    public void setSettlementRef(String AreaRef) {
        AreaRef = AreaRef;
    }
}
