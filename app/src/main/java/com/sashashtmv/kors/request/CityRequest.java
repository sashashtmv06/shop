package com.sashashtmv.kors.request;

import com.google.gson.annotations.SerializedName;

import static com.sashashtmv.kors.AppConstant.API_KEY;


public class CityRequest {
    @SerializedName("modelName")
    private String modelName;
    @SerializedName("calledMethod")
    private String calledMethod;
    @SerializedName("apiKey")
    private String apiKey;
    @SerializedName("methodProperties")
    private MethodPropertiesCity methodProperties;

    public CityRequest(String nameArea) {
        modelName = "Address";
        calledMethod = "getCities";
        methodProperties = new MethodPropertiesCity(nameArea);
        apiKey = API_KEY;
    }

    public CityRequest(String modelName, String calledMethod, String apiKey) {
        this.modelName = modelName;
        this.calledMethod = calledMethod;
        this.apiKey = apiKey;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getCalledMethod() {
        return calledMethod;
    }

    public void setCalledMethod(String calledMethod) {
        this.calledMethod = calledMethod;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
