package com.sashashtmv.kors.repository.impl;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.sashashtmv.kors.BuildConfig;
import com.sashashtmv.kors.remote.AddressApi;
import com.sashashtmv.kors.repository.RemoteRepository;
import com.sashashtmv.kors.request.AreaRequest;
import com.sashashtmv.kors.request.CityRequest;
import com.sashashtmv.kors.request.PunktRequest;
import com.sashashtmv.kors.response.DataAreaResponse;
import com.sashashtmv.kors.response.DataCityResponse;
import com.sashashtmv.kors.response.DataPunktResponse;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import static com.sashashtmv.kors.AppConstant.NovaPoshtaApi.URI_COMMON;


public class RemoteRepositoryImpl implements RemoteRepository {
    private static final boolean LOGGING = BuildConfig.DEBUG;
    private static RemoteRepositoryImpl sRemoteRepository;
    private static AddressApi mAddressApi;

    public static RemoteRepositoryImpl getInstance() {
        if (sRemoteRepository == null) {
            sRemoteRepository = new RemoteRepositoryImpl();
        }
        return sRemoteRepository;
    }

    public RemoteRepositoryImpl() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URI_COMMON)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHttpClient())
                .build();
        mAddressApi = retrofit.create(AddressApi.class);
    }

    private static OkHttpClient getHttpClient() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.followRedirects(false);
        clientBuilder.followSslRedirects(false);
        if (LOGGING) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(interceptor);
        }
        return clientBuilder.build();
    }

    @Override
    public Single<DataAreaResponse> loadAreas() {
        return mAddressApi.getAreas(new AreaRequest());
    }

    @Override
    public Single<DataCityResponse> loadCities(String nameArea) {
        return mAddressApi.getCities(new CityRequest(nameArea));
    }

    @Override
    public Single<DataPunktResponse> loadPunkts(String nameCity) {
        return mAddressApi.getPunkts(new PunktRequest(nameCity));
    }
}
