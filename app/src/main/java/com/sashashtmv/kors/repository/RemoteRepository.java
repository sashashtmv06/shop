package com.sashashtmv.kors.repository;

import com.sashashtmv.kors.response.DataAreaResponse;
import com.sashashtmv.kors.response.DataCityResponse;
import com.sashashtmv.kors.response.DataPunktResponse;

import io.reactivex.Single;


public interface RemoteRepository {
    /**
     * Load Area`s
     */
    Single<DataAreaResponse> loadAreas();

    /**
     * Load Cities
     */
    Single<DataCityResponse> loadCities(String nameArea);

    /**
     * Load Cities
     */
    Single<DataPunktResponse> loadPunkts(String nameCity);
}
