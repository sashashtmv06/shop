package com.sashashtmv.kors.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DataPunktResponse {
    @SerializedName("success")
    private boolean success;
    @SerializedName("data")
    private List<PunktResponse> mPunktResponses;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<PunktResponse> getPunktResponses() {
        return mPunktResponses;
    }

    public void setPunktResponses(List<PunktResponse> punktResponses) {
        mPunktResponses = punktResponses;
    }
}
