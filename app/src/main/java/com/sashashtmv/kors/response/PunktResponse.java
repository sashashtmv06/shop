package com.sashashtmv.kors.response;

import com.google.gson.annotations.SerializedName;

public class PunktResponse {
    @SerializedName("Description")
    private String Description;
    @SerializedName("Ref")
    private String Ref;
    @SerializedName("Phone")
    private String Phone;
    @SerializedName("TypeOfWarehouse")
    private String TypeOfWarehouse;
    @SerializedName("CityDescription")
    private String CityDescription;
    @SerializedName("Number")
    private String Number;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getRef() {
        return Ref;
    }

    public void setRef(String ref) {
        Ref = ref;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getTypeOfWarehouse() {
        return TypeOfWarehouse;
    }

    public void setTypeOfWarehouse(String typeOfWarehouse) {
        TypeOfWarehouse = typeOfWarehouse;
    }

    public String getCityDescription() {
        return CityDescription;
    }

    public void setCityDescription(String cityDescription) {
        CityDescription = cityDescription;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }
}
