package com.sashashtmv.kors.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sashashtmv.kors.R;
import com.sashashtmv.kors.model.Review;

import java.util.ArrayList;
import java.util.List;

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ReviewViewHolder> {

    private List<Review> mReviews;
    protected Review item;
    private Context mContext;

    public ReviewListAdapter(Context context, List<Review> list) {
        mContext = context;
        mReviews = list;
    }

    @NonNull
    @Override
    public ReviewListAdapter.ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReviewViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewListAdapter.ReviewViewHolder holder, int position) {
        item = mReviews.get(position);
        holder.name.setText(item.getNameUser());
        holder.text.setText(item.getTextReview());
        holder.date.setText(item.getDate());
    }

    @Override
    public int getItemCount() {
        return mReviews.size();
    }

    public class ReviewViewHolder extends RecyclerView.ViewHolder{

        protected TextView name;
        protected TextView text;
        protected TextView date;
        public ReviewViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_name);
            text = itemView.findViewById(R.id.tv_text);
            date = itemView.findViewById(R.id.tv_date);
        }
    }

    public void removeAllItems() {
        if (getItemCount() != 0) {
            mReviews = new ArrayList<>();
//            Diagnostics.i(this, "OrderAdapter_removeAllItems success" ).append("LOG_FILE");
            notifyDataSetChanged();

        }
    }

    public void addReviews(List<Review> list) {
        mReviews = list;
//        Diagnostics.i(this, "OrderAdapter_addOrdres success" ).append("LOG_FILE");
        notifyDataSetChanged();
    }
}
