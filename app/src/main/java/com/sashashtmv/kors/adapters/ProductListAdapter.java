package com.sashashtmv.kors.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.model.PreferenceHelper;
import com.sashashtmv.kors.model.Product;


import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder> {

    private List<Product> mProducts;
    protected Product item;
    private Context mContext;
    private PreferenceHelper mPreferenceHelper;

    public ProductListAdapter(Context context, List<Product> list) {
        mContext = context;
        mProducts = list;
        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        item = mProducts.get(position);
        Log.i(TAG, "onClick: like - " + item.getImageUrl());

        if(item.getImageUrl().size() > 0)
        Glide.with(mContext)
                .load(item.getImageUrl().get(0))
                .into(holder.imageView);


        holder.price.setText("₴".concat(item.getProductPrice()));
        holder.oldPrice.setText("₴".concat(item.getProductOldPrice()));
        holder.oldPrice.setPaintFlags(holder.oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.nameProduct.setText(item.getProductName());
//        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams)  holder.string.getLayoutParams();
//        params.width = holder.oldPrice.getWidth();
//        holder.string.setLayoutParams(params);
//        holder.string.getLayoutParams().width = holder.oldPrice.getWidth();
//        String url = item.getDescription();
//        Log.i(TAG, "onClick: like - " + url);
//        holder.toShop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                mContext.startActivity(browserIntent);
//            }
//        });
//        final String decscribe = item.getDescription();
//        holder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MainActivity.CommonMethod.showAlert(decscribe, (MainActivity)mContext);
//            }
//        });

        if (!item.isNew()) {
            holder.news.setVisibility(View.VISIBLE);
        } else {
            holder.news.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (mProducts != null) {
            return mProducts.size();
        } else return 0;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView nameProduct;
        TextView price;
        TextView oldPrice;
        ImageView news;


        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_id_image);
            news = itemView.findViewById(R.id.iv_icon);
            nameProduct = itemView.findViewById(R.id.tv_name_product);
            price = itemView.findViewById(R.id.tv_price);
            oldPrice = itemView.findViewById(R.id.tv_old_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            for (int i = 0; i < mProducts.size(); i++) {
                if (getAdapterPosition() == i) {
                    Log.i(TAG, "onClick: like 1 - " + item.getImageUrl());
                    MainActivity activity = (MainActivity)mContext;
                    activity.onCreateCartProductFragment(mProducts.get(i));

                }
            }
        }
    }

    public void removeAllItems() {
        if (getItemCount() != 0) {
            mProducts = new ArrayList<>();
//            Diagnostics.i(this, "OrderAdapter_removeAllItems success" ).append("LOG_FILE");
            notifyDataSetChanged();

        }
    }

    public void addShops(List<Product> list) {
        mProducts = list;
//        Diagnostics.i(this, "OrderAdapter_addOrdres success" ).append("LOG_FILE");
        notifyDataSetChanged();
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
//                            Toast.makeText(mContext, "Up button", Toast.LENGTH_LONG).show();
//                            Log.i(TAG, "onClick: return in enterfragment - " + true);
//                            Fragment fragment = mFragmentManager.findFragmentByTag("enterfragment");
//                            if (fragment != null){
//                                fragment.getFragmentManager().beginTransaction().replace(R.id.container, fragment)
//                                        .addToBackStack(null)
//                                        .commit();
//                            }
                        }
                    });
            try {
                AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                Log.i(TAG, "showAlert: - " + " false");
                e.printStackTrace();
            }

        }
    }
}
