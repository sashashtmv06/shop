package com.sashashtmv.kors.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.fragments.StartFragment;
import com.sashashtmv.kors.model.impl.Category;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder> {

    List<Category> mListCategories;
    protected Category item;
    private Context mContext;
    SharedPreferences sharedPref;
    private StartFragment.Callbacks mCallbacks;
//    OnDataChangeListener mOnDataChangeListener;
//    public interface OnDataChangeListener{
//        public void onDataChanged();
//    }
//    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener){
//        mOnDataChangeListener = onDataChangeListener;
//    }

    public CategoriesAdapter(Context context, List<Category> listCategories, StartFragment.Callbacks mCallbacks) {
        mContext = context;
        mListCategories = listCategories;
        this.mCallbacks = mCallbacks;

        sharedPref = ((MainActivity) mContext).getPreferences(Context.MODE_PRIVATE);
    }

    @Override
    public int getItemCount() {
        if (mListCategories != null) {
            return mListCategories.size();
        } else return 0;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, null));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        item = mListCategories.get(position);

        holder.nameCategory.setText(item.getName());
        holder.nameCategory.setOnClickListener(view -> mCallbacks.onCreateAllShopsFragment(mListCategories.get(holder.getPosition()).getName()));
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        Button nameCategory;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            nameCategory = itemView.findViewById(R.id.bt_category);
//            itemView.setOnClickListener(this);
        }

//        @Override
//        public void onClick(View view) {
//            mCallbacks.onCreateAllShopsFragment(nameCategory.getText().toString());
//        }
    }

}
