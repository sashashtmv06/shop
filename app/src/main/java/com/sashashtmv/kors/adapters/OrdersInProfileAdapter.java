package com.sashashtmv.kors.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.model.Order;

import java.util.List;

public class OrdersInProfileAdapter extends RecyclerView.Adapter<OrdersInProfileAdapter.OrderViewHolder> {

    private List<Order> orderList;
    protected Order item;
    private Context mContext;
    OnDataChangeListener mOnDataChangeListener;
    public interface OnDataChangeListener{
        public void onDataChanged();
    }
    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener){
        mOnDataChangeListener = onDataChangeListener;
    }

    public OrdersInProfileAdapter(Context context, List<Order> list) {
        mContext = context;
        orderList = list;
    }

    @NonNull
    @Override
    public OrdersInProfileAdapter.OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_order_in_profile, null));
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersInProfileAdapter.OrderViewHolder holder, int position) {
        item = orderList.get(position);

        holder.price.setText(item.getPrice());
        holder.nameProduct.setText(item.getStoreName());
        holder.countProduct.setText(item.getCount());
        holder.status.setText(item.getStatus());
        holder.date.setText(item.getDate());
        holder.idSale.setText(item.getId());
//        holder.date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int count = Integer.parseInt(holder.countProduct.getText().toString());
//                item = orderList.get(holder.getAdapterPosition());
//                count++;
//                sharedPref.edit().putInt(String.valueOf(item.getProductId()), count).apply();
//                holder.countProduct.setText(String.valueOf(count));
//                holder.priceTwo.setText("₴".concat(String.valueOf(count * Double.parseDouble(item.getProductPrice()))));
//                holder.sum.setText("₴".concat(String.valueOf(count * Double.parseDouble(item.getProductPrice()))));
//                if(mOnDataChangeListener != null){
//                    mOnDataChangeListener.onDataChanged();
//                }
//            }
//        });
//        holder.idSale.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                sharedPref.edit().putInt(String.valueOf(item.getProductId()), 0).apply();
//                orderList.remove(holder.getAdapterPosition());
//                notifyDataSetChanged();
//                if(mOnDataChangeListener != null){
//                    mOnDataChangeListener.onDataChanged();
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        TextView nameProduct;
        TextView price;
        TextView countProduct;
        TextView status;
        TextView date;
        TextView idSale;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            nameProduct = itemView.findViewById(R.id.tv_name_od_shop);
            price = itemView.findViewById(R.id.tv_price);
            countProduct = itemView.findViewById(R.id.tv_count);
            status = itemView.findViewById(R.id.tv_status);
            date = itemView.findViewById(R.id.tv_date);
            idSale = itemView.findViewById(R.id.tv_id_orders);
        }

    }

}
