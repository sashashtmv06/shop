package com.sashashtmv.kors.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.model.Order;
import com.sashashtmv.kors.model.Product;

import java.util.List;

import static android.content.ContentValues.TAG;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrderViewHolder> {

    private List<Product> orderList;
    protected Product item;
    private Context mContext;
    SharedPreferences sharedPref;
    OnDataChangeListener mOnDataChangeListener;
    public interface OnDataChangeListener{
        public void onDataChanged();
    }
    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener){
        mOnDataChangeListener = onDataChangeListener;
    }

    public OrdersAdapter(Context context, List<Product> list) {
        mContext = context;
        orderList = list;
        sharedPref = ((MainActivity) mContext).getPreferences(Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public OrdersAdapter.OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_order, null));
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersAdapter.OrderViewHolder holder, int position) {
        item = orderList.get(position);
        Glide.with(mContext)
                .load(item.getImageUrl().get(0))
                .into(holder.imageView);


        holder.price.setText("₴".concat(item.getProductPrice()));
        holder.oldPrice.setText("₴".concat(item.getProductOldPrice()));
        holder.oldPrice.setPaintFlags(holder.oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.nameProduct.setText(item.getProductName());
        int count = sharedPref.getInt(String.valueOf(item.getProductId()), 0);
        holder.sum.setText(String.valueOf(count * Double.parseDouble(item.getProductPrice())));
        holder.countProduct.setText(String.valueOf(count));
        holder.priceTwo.setText("₴".concat(String.valueOf(count * Double.parseDouble(item.getProductPrice()))));
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = Integer.parseInt(holder.countProduct.getText().toString());
                count--;
                if (count < 0) count = 0;
                item = orderList.get(holder.getAdapterPosition());
                sharedPref.edit().putInt(String.valueOf(item.getProductId()), count).apply();
                holder.countProduct.setText(String.valueOf(count));
                holder.priceTwo.setText("₴".concat(String.valueOf(count * Double.parseDouble(item.getProductPrice()))));
                holder.sum.setText("₴".concat(String.valueOf(count * Double.parseDouble(item.getProductPrice()))));
                if(mOnDataChangeListener != null){
                    mOnDataChangeListener.onDataChanged();
                }
            }
        });
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = Integer.parseInt(holder.countProduct.getText().toString());
                item = orderList.get(holder.getAdapterPosition());
                count++;
                sharedPref.edit().putInt(String.valueOf(item.getProductId()), count).apply();
                holder.countProduct.setText(String.valueOf(count));
                holder.priceTwo.setText("₴".concat(String.valueOf(count * Double.parseDouble(item.getProductPrice()))));
                holder.sum.setText("₴".concat(String.valueOf(count * Double.parseDouble(item.getProductPrice()))));
                if(mOnDataChangeListener != null){
                    mOnDataChangeListener.onDataChanged();
                }
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPref.edit().putInt(String.valueOf(item.getProductId()), 0).apply();
                orderList.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
                if(mOnDataChangeListener != null){
                    mOnDataChangeListener.onDataChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView nameProduct;
        TextView price;
        TextView priceTwo;
        TextView oldPrice;
        TextView countProduct;
        ImageButton minus;
        ImageButton plus;
        ImageButton delete;
        TextView sum;


        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.iv_id_image);
            nameProduct = itemView.findViewById(R.id.tv_name_product);
            price = itemView.findViewById(R.id.tv_price);
            priceTwo = itemView.findViewById(R.id.tv_price_two);
            oldPrice = itemView.findViewById(R.id.tv_old_price);
            countProduct = itemView.findViewById(R.id.tv_count);
            minus = itemView.findViewById(R.id.ib_minus);
            plus = itemView.findViewById(R.id.ib_plus);
            delete = itemView.findViewById(R.id.ib_delete_product);
            sum = itemView.findViewById(R.id.tv_sum);
        }

    }

}
