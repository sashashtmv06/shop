package com.sashashtmv.kors.adapters;

import android.app.FragmentManager;
import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.fragments.ArticleFragment;
import com.sashashtmv.kors.model.Article;

import java.net.URL;
import java.util.List;

import static android.content.ContentValues.TAG;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ArticleViewHolder> {

    private List<Article> articleList;
    protected Article item;
    private Context mContext;
    private ArticleFragment articleFragment;
    private FragmentManager mFragmentManager;

    public ArticlesAdapter(Context context, List<Article> list) {
        mContext = context;
        articleList = list;
        mFragmentManager = ((MainActivity)context).getFragmentManager();
    }

    @NonNull
    @Override
    public ArticlesAdapter.ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ArticleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_article, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ArticlesAdapter.ArticleViewHolder holder, int position) {
        item = articleList.get(position);
        Glide.with(mContext)
                .load(String.valueOf(item.getImageUrl()))
                .into(holder.imageView);

        holder.titleArticles.setText(item.getTitle());
        holder.text = item.getText();
        holder.title = item.getTitle();
        holder.image = item.getImageUrl();
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imageView;
        TextView titleArticles;
        URL image;
        String title;
        String text;


        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_id_image);
            titleArticles = itemView.findViewById(R.id.tv_title_article);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    articleFragment = ArticleFragment.newInstance(articleList.get(position).getImageUrl(), articleList.get(position).getTitle(), articleList.get(position).getText());
                    mFragmentManager.beginTransaction()
                            .replace(R.id.container, articleFragment, "articleFragment")
                            .addToBackStack(null)
                            .commit();
                }
            });

        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "onClick: - " + "true");
            articleFragment = ArticleFragment.newInstance(image, title, text);
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, articleFragment, "articleFragment")
                    .addToBackStack(null)
                    .commit();
        }
    }
}
