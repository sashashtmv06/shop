package com.sashashtmv.kors.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sashashtmv.kors.MainActivity;
import com.sashashtmv.kors.R;
import com.sashashtmv.kors.model.Product;

import java.util.List;

public class CompleteOrdersAdapter extends RecyclerView.Adapter<CompleteOrdersAdapter.OrderViewHolder> {

    private List<Product> orderList;
    protected Product item;
    private Context mContext;
    SharedPreferences sharedPref;
//    OnDataChangeListener mOnDataChangeListener;
//    public interface OnDataChangeListener{
//        public void onDataChanged();
//    }
//    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener){
//        mOnDataChangeListener = onDataChangeListener;
//    }

    public CompleteOrdersAdapter(Context context, List<Product> list) {
        mContext = context;
        orderList = list;
        sharedPref = ((MainActivity) mContext).getPreferences(Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public CompleteOrdersAdapter.OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_complete_order, null));
    }

    @Override
    public void onBindViewHolder(@NonNull CompleteOrdersAdapter.OrderViewHolder holder, int position) {
        item = orderList.get(position);
        Glide.with(mContext)
                .load(item.getImageUrl().get(0))
                .into(holder.imageView);

        holder.nameProduct.setText(item.getProductName());
        int count = sharedPref.getInt(String.valueOf(item.getProductId()), 0);
        holder.sum.setText("₴".concat(String.valueOf(count * Double.parseDouble(item.getProductPrice()))));
        holder.countProduct.setText(String.valueOf(count));
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView nameProduct;
        TextView countProduct;
        TextView sum;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_id_image);
            nameProduct = itemView.findViewById(R.id.tv_name_product);
            countProduct = itemView.findViewById(R.id.tv_count);
            sum = itemView.findViewById(R.id.tv_sum);
        }

    }

}
