package com.sashashtmv.kors.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIInterface {

    @FormUrlEncoded
    @POST("register")
    Call<Object> createUser(
            @Field("phone") String phone,
            @Field("password") String password,
            @Field("name") String name,
            @Field("surname") String surname,
            @Field("patronymic") String patronymic,
            @Field("device_token") String device_token,
            @Field("fcm_token") String fcm_token);

    @Headers("Content-Type: application/json")
    @POST("login")
    Call<Object> createLogin(@Body String body);

    @FormUrlEncoded
    @POST("verify/send_code")
    Call<Object> sendVerifyCode(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("verify/check_code")
    Call<Object> createValidation(
            @Field("phone") String phone,
            @Field("code") String code);

    @FormUrlEncoded
    @POST("register/pwd_reset")
    Call<Object> resetPassword(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("reviews/add")
    Call<Object> sendReview(@Header("Authorization") String tokenString, @Field("text") String text);

    @GET("reviews/list")
    Call<Object> getReviews(@Header("Authorization") String tokenString);

    @GET("category/all_published")
    Call<Object> getCategories();

    @GET("product/by_category/{categoryID}")
    Call<Object> getProducts(@Path("categoryID") String categoryID);


    @POST("shops/list")
    Call<Object> getShops(@Header("Authorization") String tokenString);

    @FormUrlEncoded
    @POST("articles/list")
    Call<Object> getArticles(@Header("Authorization") String tokenString, @Field("type") String text);

    @FormUrlEncoded
    @POST("support/new")
    Call<Object> sendQuestion(@Header("Authorization") String tokenString, @Field("question") String text);

    @GET("orders/list")
    Call<Object> getOrders(@Header("Authorization") String tokenString);

    @GET("orders/balance")
    Call<Object> getBalance(@Header("Authorization") String tokenString);

    @FormUrlEncoded
    @POST("orders/withdraw")
    Call<Object> withdrawals(@Header("Authorization") String tokenString,
                             @Field("sumToWithdraw") String sumToWithdraw,
                             @Field("type") String type,
                             @Field("destination") String destination);

    @Headers("Content-Type: application/json")
    @POST("sale/new")
    Call<Object> sendOrder(
            @Header("Authorization") String tokenString,
            @Body String body);

    @GET("sale/all")
    Call<Object> getUserOrders(@Header("Authorization") String tokenString);

    @GET("user_info")
    Call<Object> getUserInfo(@Header("Authorization") String tokenString);

    @FormUrlEncoded
    @POST("user/edit")
    Call<Object> updateInfoUser(@Header("Authorization") String tokenString,
                                @Field("password") String password,
                                @Field("email") String email,
                                @Field("phone") String phone,
                                @Field("name") String name,
                                @Field("surname") String surname,
                                @Field("patronymic") String patronymic);

}
